-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 13, 2014 at 04:38 PM
-- Server version: 5.5.37
-- PHP Version: 5.5.11-3+deb.sury.org~precise+1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shareads`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `object` varchar(55) NOT NULL,
  `action` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `value` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`object`,`action`,`role_id`),
  KEY `fki-access-roles-role_id-id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`object`, `action`, `role_id`, `value`) VALUES
('AdminArea', 'access', 1, 'allow'),
('AdminArea', 'access', 2, 'deny'),
('AdminArea', 'access', 3, 'deny');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `widget_id` int(11) NOT NULL,
  `widget_order` int(5) NOT NULL,
  `layout` varchar(50) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-content-widgets-widget_id-id` (`widget_id`),
  KEY `fki-content-pages-page_id-id` (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `page_id`, `widget_id`, `widget_order`, `layout`, `params`) VALUES
(7, 3, 1, 1, 'middle', '{"title":null,"html_en":"<table align=\\"center\\" border=\\"0\\" style=\\"color:rgb(92, 92, 92); font-family:arial; font-size:12px; height:417px; line-height:normal; width:744px\\">\\r\\n\\t<tbody>\\r\\n\\t\\t<tr>\\r\\n\\t\\t\\t<td colspan=\\"3\\">\\r\\n\\t\\t\\t<p style=\\"text-align:center\\"><strong>\\"Where your Membership is WORTH SOMETHING!\\"<\\/strong><img src=\\"http:\\/\\/shareads.us\\/mainimages\\/servicemark.png\\" \\/>\\u00a0<br \\/>\\r\\n\\t\\t\\t<br \\/>\\r\\n\\t\\t\\t<img src=\\"http:\\/\\/shareads.us\\/mainimages\\/ShareADsus_long3Dpink.png\\" \\/>\\u00a0<br \\/>\\r\\n\\t\\t\\t<a href=\\"http:\\/\\/www.shareads.us\\/\\"><img src=\\"http:\\/\\/shareads.us\\/mainimages\\/3worldspurpleNEWclear1.png\\" \\/><\\/a><\\/p>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t<\\/tr>\\r\\n\\t\\t<tr>\\r\\n\\t\\t\\t<td>\\r\\n\\t\\t\\t<div style=\\"text-align: center;\\"><a href=\\"http:\\/\\/shareads.us\\/jobs.php\\"><img src=\\"http:\\/\\/shareads.us\\/mainimages\\/Jobsforanyoneandeveryone.png\\" \\/><\\/a><\\/div>\\r\\n\\t\\t\\t\\u00a0\\r\\n\\r\\n\\t\\t\\t<div style=\\"text-align: center;\\"><img src=\\"http:\\/\\/shareads.us\\/mainimages\\/Anideacantransform.jpg\\" style=\\"height:35px; width:600px\\" \\/>\\u00a0<br \\/>\\r\\n\\t\\t\\tfrom the movie, &#39;Inception&#39;<\\/div>\\r\\n\\r\\n\\t\\t\\t<div>\\u00a0<\\/div>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t<\\/tr>\\r\\n\\t<\\/tbody>\\r\\n<\\/table>\\r\\n","roles":null}'),
(9, 1, 3, 1, 'middle', '{"logo":"files\\/logo.png","show_title":0,"show_auth":"1","roles":null}'),
(10, 1, 2, 2, 'middle', '{"title":"","class":"","menu":"Default menu","menu_id":"1","roles":null,"content_id":"10"}'),
(11, 2, 1, 1, 'middle', '{"title":null,"html_en":"<p style=\\"text-align:center\\">\\u00a9 2014 Shareads.us. All Rights Reserved.<\\/p>\\r\\n","roles":null}');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `language` varchar(2) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `language`, `locale`, `icon`) VALUES
(1, 'English', 'en', 'en_US', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language_translations`
--

CREATE TABLE IF NOT EXISTS `language_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `scope` varchar(25) DEFAULT NULL,
  `original` text NOT NULL,
  `translated` text NOT NULL,
  `checked` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-language_translations-languages-language_id-id` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=236 ;

--
-- Dumping data for table `language_translations`
--

INSERT INTO `language_translations` (`id`, `language_id`, `scope`, `original`, `translated`, `checked`) VALUES
(1, 1, 'core', 'Home', 'Home', 0),
(2, 1, 'core', 'Are you really want to delete this item?', 'Are you really want to delete this item?', 0),
(3, 1, 'core', 'Close this window?', 'Close this window?', 0),
(4, 1, 'core', 'PhalconEye', 'PhalconEye', 0),
(5, 1, 'core', 'PhalconEye Home Page', 'PhalconEye Home Page', 0),
(6, 1, 'core', 'Login', 'Login', 0),
(7, 1, 'core', 'Register', 'Register', 0),
(8, 1, 'core', 'Github', 'Github', 0),
(9, 1, 'core', 'Header', 'Header', 0),
(10, 1, 'core', 'Header2', 'Header2', 0),
(11, 1, 'core', 'Left', 'Left', 0),
(12, 1, 'core', 'Left2', 'Left2', 0),
(13, 1, 'core', 'Right', 'Right', 0),
(14, 1, 'core', 'Right2', 'Right2', 0),
(15, 1, 'core', 'Content', 'Content', 0),
(16, 1, 'core', 'Content2', 'Content2', 0),
(17, 1, 'user', 'Use you email or username to login.', 'Use you email or username to login.', 0),
(18, 1, 'user', 'Password', 'Password', 0),
(19, 1, 'user', 'Enter', 'Enter', 0),
(20, 1, 'core', 'Welcome, ', 'Welcome, ', 0),
(21, 1, 'core', 'Admin panel', 'Admin panel', 0),
(22, 1, 'core', 'Logout', 'Logout', 0),
(23, 1, 'core', 'Dashboard', 'Dashboard', 0),
(24, 1, 'core', 'Manage', 'Manage', 0),
(25, 1, 'core', 'Users and Roles', 'Users and Roles', 0),
(26, 1, 'core', 'Pages', 'Pages', 0),
(27, 1, 'core', 'Menus', 'Menus', 0),
(28, 1, 'core', 'Languages', 'Languages', 0),
(29, 1, 'core', 'Files', 'Files', 0),
(30, 1, 'core', 'Packages', 'Packages', 0),
(31, 1, 'core', 'Settings', 'Settings', 0),
(32, 1, 'core', 'System', 'System', 0),
(33, 1, 'core', 'Performance', 'Performance', 0),
(34, 1, 'core', 'Access Rights', 'Access Rights', 0),
(35, 1, 'core', 'Back to site', 'Back to site', 0),
(36, 1, 'core', 'Debug mode', 'Debug mode', 0),
(37, 1, 'user', 'ID', 'ID', 0),
(38, 1, 'user', 'Username', 'Username', 0),
(39, 1, 'user', 'Email', 'Email', 0),
(40, 1, 'user', 'Role', 'Role', 0),
(41, 1, 'user', 'Creation Date', 'Creation Date', 0),
(42, 1, 'user', 'Users', 'Users', 0),
(43, 1, 'user', 'Are you really want to delete this user?', 'Are you really want to delete this user?', 0),
(44, 1, 'user', 'Roles', 'Roles', 0),
(45, 1, 'user', 'Create new user', 'Create new user', 0),
(46, 1, 'user', 'Create new role', 'Create new role', 0),
(47, 1, 'user', 'Actions', 'Actions', 0),
(48, 1, 'user', 'Admin', 'Admin', 0),
(49, 1, 'user', 'User', 'User', 0),
(50, 1, 'user', 'Guest', 'Guest', 0),
(51, 1, 'user', 'Filter', 'Filter', 0),
(52, 1, 'user', 'Reset', 'Reset', 0),
(53, 1, 'user', 'Edit', 'Edit', 0),
(54, 1, 'user', 'Delete', 'Delete', 0),
(55, 1, 'core', 'Title', 'Title', 0),
(56, 1, 'core', 'Url', 'Url', 0),
(57, 1, 'core', 'Layout', 'Layout', 0),
(58, 1, 'core', 'Controller', 'Controller', 0),
(59, 1, 'core', 'Are you really want to delete this page?', 'Are you really want to delete this page?', 0),
(60, 1, 'core', 'Browse', 'Browse', 0),
(61, 1, 'core', 'Create new page', 'Create new page', 0),
(62, 1, 'core', 'Menu title', 'Menu title', 0),
(63, 1, 'core', 'Create new menu', 'Create new menu', 0),
(64, 1, 'core', 'Name', 'Name', 0),
(65, 1, 'core', 'Language', 'Language', 0),
(66, 1, 'core', 'Locale', 'Locale', 0),
(67, 1, 'core', 'Icon', 'Icon', 0),
(68, 1, 'core', 'Create new language', 'Create new language', 0),
(69, 1, 'core', 'Compile languages', 'Compile languages', 0),
(70, 1, 'core', 'Import...', 'Import...', 0),
(71, 1, 'core', 'File', 'File', 0),
(72, 1, 'core', 'No icon', 'No icon', 0),
(73, 1, 'core', 'Export', 'Export', 0),
(74, 1, 'core', 'Files management', 'Files management', 0),
(75, 1, 'core', 'Packages management - Modules', 'Packages management - Modules', 0),
(76, 1, 'core', 'Are you really want to remove this package? Once removed, it can not be restored.', 'Are you really want to remove this package? Once removed, it can not be restored.', 0),
(77, 1, 'core', 'Modules', 'Modules', 0),
(78, 1, 'core', 'Themes', 'Themes', 0),
(79, 1, 'core', 'Widgets', 'Widgets', 0),
(80, 1, 'core', 'Plugins', 'Plugins', 0),
(81, 1, 'core', 'Libraries', 'Libraries', 0),
(82, 1, 'core', 'Upload new package', 'Upload new package', 0),
(83, 1, 'core', 'Create new package', 'Create new package', 0),
(84, 1, 'core', 'System settings', 'System settings', 0),
(85, 1, 'core', 'All system settings here.', 'All system settings here.', 0),
(86, 1, 'core', 'Site name', 'Site name', 0),
(87, 1, 'core', 'Theme', 'Theme', 0),
(88, 1, 'core', 'Default', 'Default', 0),
(89, 1, 'core', 'Shareads', 'Shareads', 0),
(90, 1, 'core', 'Default language', 'Default language', 0),
(91, 1, 'core', 'Auto detect', 'Auto detect', 0),
(92, 1, 'core', 'English', 'English', 0),
(93, 1, 'core', 'Save', 'Save', 0),
(94, 1, 'core', 'Performance settings', 'Performance settings', 0),
(95, 1, 'core', 'Cache prefix', 'Cache prefix', 0),
(96, 1, 'core', 'Example: "pe_"', 'Example: "pe_"', 0),
(97, 1, 'core', 'Cache lifetime', 'Cache lifetime', 0),
(98, 1, 'core', 'This determines how long the system will keep cached data before                    reloading it from the database server.                    A shorter cache lifetime causes greater database server CPU usage,                    however the data will be more current.', 'This determines how long the system will keep cached data before                    reloading it from the database server.                    A shorter cache lifetime causes greater database server CPU usage,                    however the data will be more current.', 0),
(99, 1, 'core', 'Cache adapter', 'Cache adapter', 0),
(100, 1, 'core', 'Cache type. Where cache will be stored.', 'Cache type. Where cache will be stored.', 0),
(101, 1, 'core', 'Memcached', 'Memcached', 0),
(102, 1, 'core', 'APC', 'APC', 0),
(103, 1, 'core', 'Mongo', 'Mongo', 0),
(104, 1, 'core', 'Files location', 'Files location', 0),
(105, 1, 'core', 'Memcached host', 'Memcached host', 0),
(106, 1, 'core', 'Memcached port', 'Memcached port', 0),
(107, 1, 'core', 'Create a persistent connection to memcached?', 'Create a persistent connection to memcached?', 0),
(108, 1, 'core', 'A MongoDB connection string', 'A MongoDB connection string', 0),
(109, 1, 'core', 'Mongo database name', 'Mongo database name', 0),
(110, 1, 'core', 'Mongo collection in the database', 'Mongo collection in the database', 0),
(111, 1, 'core', 'Clear cache', 'Clear cache', 0),
(112, 1, 'core', 'All system cache will be cleaned.', 'All system cache will be cleaned.', 0),
(113, 1, 'core', 'Not Found', 'Not Found', 0),
(114, 1, 'core', 'Wrong controller name. Example: NameController->someAction', 'Wrong controller name. Example: NameController->someAction', 0),
(115, 1, 'core', 'Page Creation', 'Page Creation', 0),
(116, 1, 'core', 'Create new page.', 'Create new page.', 0),
(117, 1, 'core', 'Page will be available under http://shareads.dev:8080/page/[URL NAME]', 'Page will be available under http://shareads.dev:8080/page/[URL NAME]', 0),
(118, 1, 'core', 'Description', 'Description', 0),
(119, 1, 'core', 'Keywords', 'Keywords', 0),
(120, 1, 'core', 'Controller and action name that will handle this page. Example: NameController->someAction', 'Controller and action name that will handle this page. Example: NameController->someAction', 0),
(121, 1, 'core', 'If no value is selected, will be allowed to all (also as all selected).', 'If no value is selected, will be allowed to all (also as all selected).', 0),
(122, 1, 'core', 'Create', 'Create', 0),
(123, 1, 'core', 'Cancel', 'Cancel', 0),
(124, 1, 'core', 'Name must be in lowercase and contains only letters.', 'Name must be in lowercase and contains only letters.', 0),
(125, 1, 'core', 'Version must be in correct format: 1.0.0 or 1.0.0.0', 'Version must be in correct format: 1.0.0 or 1.0.0.0', 0),
(126, 1, 'core', 'Package Creation', 'Package Creation', 0),
(127, 1, 'core', 'Create new package.', 'Create new package.', 0),
(128, 1, 'core', 'Package type', 'Package type', 0),
(129, 1, 'core', 'Module', 'Module', 0),
(130, 1, 'core', 'Plugin', 'Plugin', 0),
(131, 1, 'core', 'Widget', 'Widget', 0),
(132, 1, 'core', 'Library', 'Library', 0),
(133, 1, 'core', 'Version', 'Version', 0),
(134, 1, 'core', 'Type package version. Ex.: 0.5.7', 'Type package version. Ex.: 0.5.7', 0),
(135, 1, 'core', 'Author', 'Author', 0),
(136, 1, 'core', 'Who create this package? Identify yourself!', 'Who create this package? Identify yourself!', 0),
(137, 1, 'core', 'Website', 'Website', 0),
(138, 1, 'core', 'Where user will look for new version?', 'Where user will look for new version?', 0),
(139, 1, 'core', 'Header comments', 'Header comments', 0),
(140, 1, 'core', 'This text will be placed in each file of package. Use comment block /**  **/.', 'This text will be placed in each file of package. Use comment block /**  **/.', 0),
(141, 1, 'core', 'Widget information', 'Widget information', 0),
(142, 1, 'core', 'Is related to module?', 'Is related to module?', 0),
(143, 1, 'core', 'No', 'No', 0),
(144, 1, 'core', 'Core', 'Core', 0),
(145, 1, 'core', 'Is Paginated?', 'Is Paginated?', 0),
(146, 1, 'core', 'If this enabled - widget will has additional control                    enabled for allowed per page items count selection in admin form', 'If this enabled - widget will has additional control                    enabled for allowed per page items count selection in admin form', 0),
(147, 1, 'core', 'Is ACL controlled?', 'Is ACL controlled?', 0),
(148, 1, 'core', 'If this enabled - widget will has additional control                    enabled for allowed roles selection in admin form', 'If this enabled - widget will has additional control                    enabled for allowed roles selection in admin form', 0),
(149, 1, 'core', 'Admin form', 'Admin form', 0),
(150, 1, 'core', 'Does this widget have some controlling form?', 'Does this widget have some controlling form?', 0),
(151, 1, 'core', 'Action', 'Action', 0),
(152, 1, 'core', 'Form class', 'Form class', 0),
(153, 1, 'core', 'Enter existing form class', 'Enter existing form class', 0),
(154, 1, 'core', 'Enabled?', 'Enabled?', 0),
(155, 1, 'core', 'Member', 'Member', 0),
(156, 1, 'core', 'Events', 'Events', 0),
(157, 1, 'core', 'Disable', 'Disable', 0),
(158, 1, 'core', 'Uninstall', 'Uninstall', 0),
(159, 1, 'core', 'Edit package', 'Edit package', 0),
(160, 1, 'core', 'Edit this package.', 'Edit this package.', 0),
(161, 1, 'core', 'Module settings', 'Module settings', 0),
(162, 1, 'core', 'This module has no settings...', 'This module has no settings...', 0),
(163, 1, 'core', 'Membership', 'Membership', 0),
(164, 1, 'membership', 'Sign up', 'Sign up', 0),
(165, 1, 'membership', 'Sign up new user.', 'Sign up new user.', 0),
(167, 1, 'user', 'Incorrect email or password!', 'Incorrect email or password!', 0),
(168, 1, 'core', 'Page not saved! Dou you want to leave?', 'Page not saved! Dou you want to leave?', 0),
(169, 1, 'core', 'Error while saving...', 'Error while saving...', 0),
(170, 1, 'core', 'Save (NOT  SAVED)', 'Save (NOT  SAVED)', 0),
(171, 1, 'core', 'Footer', 'Footer', 0),
(172, 1, 'core', 'If you switch to new layout you will lose some widgets, are you shure?', 'If you switch to new layout you will lose some widgets, are you shure?', 0),
(173, 1, 'core', 'Manage page', 'Manage page', 0),
(174, 1, 'core', 'Change layout', 'Change layout', 0),
(175, 1, 'core', 'Select layout type for current page', 'Select layout type for current page', 0),
(176, 1, 'core', 'Saving...', 'Saving...', 0),
(177, 1, 'core', 'Settings for header of you site.', 'Settings for header of you site.', 0),
(178, 1, 'core', 'Logo image (url)', 'Logo image (url)', 0),
(179, 1, 'core', 'Select file', 'Select file', 0),
(180, 1, 'core', 'Show site title', 'Show site title', 0),
(181, 1, 'core', 'Show authentication links (logo, register, logout, etc)', 'Show authentication links (logo, register, logout, etc)', 0),
(182, 1, 'core', 'Close', 'Close', 0),
(183, 1, 'core', 'Menu', 'Menu', 0),
(184, 1, 'core', 'Select menu that will be rendered.', 'Select menu that will be rendered.', 0),
(185, 1, 'core', 'Menu css class', 'Menu css class', 0),
(186, 1, 'core', 'Start typing to see menus variants', 'Start typing to see menus variants', 0),
(187, 1, 'core', 'HTML block, for:', 'HTML block, for:', 0),
(188, 1, 'core', 'HtmlBlock', 'HtmlBlock', 0),
(189, 1, 'core', 'HTML (EN)', 'HTML (EN)', 0),
(190, 1, 'core', 'Manage menu', 'Manage menu', 0),
(191, 1, 'core', 'Add new item', 'Add new item', 0),
(192, 1, 'core', 'Saved...', 'Saved...', 0),
(193, 1, 'core', 'Items: ', 'Items: ', 0),
(194, 1, 'core', 'Remove', 'Remove', 0),
(195, 1, 'core', 'Edit menu item', 'Edit menu item', 0),
(196, 1, 'core', 'This menu item will be available under menu or parent menu item.', 'This menu item will be available under menu or parent menu item.', 0),
(197, 1, 'core', 'Target', 'Target', 0),
(198, 1, 'core', 'Link type', 'Link type', 0),
(199, 1, 'core', 'Default link', 'Default link', 0),
(200, 1, 'core', 'Opens the linked document in a new window or tab', 'Opens the linked document in a new window or tab', 0),
(201, 1, 'core', 'Opens the linked document in the parent frame', 'Opens the linked document in the parent frame', 0),
(202, 1, 'core', 'Opens the linked document in the full body of the window', 'Opens the linked document in the full body of the window', 0),
(203, 1, 'core', 'Select url type', 'Select url type', 0),
(204, 1, 'core', 'System page', 'System page', 0),
(205, 1, 'core', 'Do not type url with starting slash... Example: "somepage/url/to?param=1"', 'Do not type url with starting slash... Example: "somepage/url/to?param=1"', 0),
(206, 1, 'core', 'Page', 'Page', 0),
(207, 1, 'core', 'Start typing to see pages variants.', 'Start typing to see pages variants.', 0),
(208, 1, 'core', 'OnClick', 'OnClick', 0),
(209, 1, 'core', 'Type JS action that will be performed when this menu item is selected.', 'Type JS action that will be performed when this menu item is selected.', 0),
(210, 1, 'core', 'Tooltip', 'Tooltip', 0),
(211, 1, 'core', 'Tooltip position', 'Tooltip position', 0),
(212, 1, 'core', 'Top', 'Top', 0),
(213, 1, 'core', 'Bottom', 'Bottom', 0),
(214, 1, 'core', 'Select icon', 'Select icon', 0),
(215, 1, 'core', 'Icon position', 'Icon position', 0),
(216, 1, 'core', 'Choose the language in which the menu item will be displayed.                    If no one selected - will be displayed at all.', 'Choose the language in which the menu item will be displayed.                    If no one selected - will be displayed at all.', 0),
(217, 1, 'core', 'Is enabled', 'Is enabled', 0),
(218, 1, 'core', 'Create new menu item', 'Create new menu item', 0),
(219, 1, 'core', 'Packages management - Plugins', 'Packages management - Plugins', 0),
(220, 1, 'core', 'No packages', 'No packages', 0),
(221, 1, 'core', 'Add new', 'Add new', 0),
(222, 1, 'core', 'Event', 'Event', 0),
(223, 1, 'core', 'Class', 'Class', 0),
(224, 1, 'core', 'Edit package events', 'Edit package events', 0),
(225, 1, 'core', '                Edit events.<br/>                In "Event" field write event name. Example: dispatch:beforeDispatchLoop<br/>                In "Class" field write plugin class with namespace.                Example: Plugin\\SomePlugin\\Some                ', '                Edit events.<br/>                In "Event" field write event name. Example: dispatch:beforeDispatchLoop<br/>                In "Class" field write plugin class with namespace.                Example: Plugin\\SomePlugin\\Some                ', 0),
(226, 1, 'membership', 'Firstname', 'Firstname', 0),
(227, 1, 'membership', 'Lastname', 'Lastname', 0),
(228, 1, 'membership', 'Field email must be an email address', 'Field email must be an email address', 0),
(229, 1, 'membership', 'Field firstname is required', 'Field firstname is required', 0),
(230, 1, 'membership', 'Field lastname is required', 'Field lastname is required', 0),
(231, 1, 'user', 'User Editing', 'User Editing', 0),
(232, 1, 'user', 'Edit User', 'Edit User', 0),
(233, 1, 'user', 'Edit this user.', 'Edit this user.', 0),
(234, 1, 'user', 'Select user role', 'Select user role', 0),
(235, 1, 'core', 'Home Page', 'Home Page', 0);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`) VALUES
(1, 'Default menu');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `onclick` varchar(255) DEFAULT NULL,
  `target` varchar(10) DEFAULT NULL,
  `tooltip` varchar(255) DEFAULT NULL,
  `tooltip_position` varchar(10) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_position` varchar(10) NOT NULL,
  `languages` varchar(150) DEFAULT NULL,
  `roles` varchar(150) DEFAULT NULL,
  `is_enabled` tinyint(1) DEFAULT NULL,
  `item_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fki-menu_items-menus-menu_id-id` (`menu_id`),
  KEY `fki-menu_items-menu_items-parent_id-id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `title`, `menu_id`, `parent_id`, `page_id`, `url`, `onclick`, `target`, `tooltip`, `tooltip_position`, `icon`, `icon_position`, `languages`, `roles`, `is_enabled`, `item_order`) VALUES
(1, 'Home', 1, NULL, NULL, '/', NULL, NULL, NULL, 'top', NULL, 'left', NULL, NULL, 1, 0),
(2, 'Github', 1, NULL, NULL, 'https://github.com/lantian/PhalconEye', NULL, '_blank', '<p><strong><span style="color:#FF0000">G</span>it<span style="color:#FF0000">H</span>ub Page</strong></p>\r\n', 'right', 'files/github.gif', 'left', NULL, NULL, 0, 1),
(3, 'Sign up', 1, NULL, NULL, 'members/signup', NULL, NULL, NULL, 'top', NULL, 'left', NULL, NULL, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `type` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `version` varchar(32) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `type`, `title`, `description`, `version`, `author`, `website`, `enabled`, `is_system`, `data`) VALUES
(1, 'core', 'module', 'Core', 'PhalconEye Core', '0.4.0', 'PhalconEye Team', 'http://phalconeye.com/', 1, 1, NULL),
(2, 'user', 'module', 'Users', 'PhalconEye Users', '0.4.0', 'PhalconEye Team', 'http://phalconeye.com/', 1, 1, NULL),
(3, 'membership', 'module', 'Membership', 'Member Management', '0.0.1', 'Quynh', 'http://www.shareads.us', 1, 0, '{"events":[],"widgets":[{"name":"Demowidget","module":"membership","description":"test","is_paginated":false,"is_acl_controlled":false,"admin_form":"action","enabled":true}]}'),
(4, 'profile', 'plugin', 'Profile', NULL, '0.0.1', 'Quynh Le', 'http://www.shareads.us', 1, 0, '{"events":[],"widgets":[]}'),
(5, 'demowidget', 'widget', 'demo', 'test', '0.0.1', 'Quynh Le', 'http://www.shareads.us', 1, 0, '{"module":"membership","widget_id":"4"}');

-- --------------------------------------------------------

--
-- Table structure for table `package_dependencies`
--

CREATE TABLE IF NOT EXISTS `package_dependencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `dependency_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-package_dependencies-packages-package_id-id` (`package_id`),
  KEY `fki-package_dependencies-packages-dependency_id-id` (`dependency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `package_dependencies`
--

INSERT INTO `package_dependencies` (`id`, `package_id`, `dependency_id`) VALUES
(1, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `type` varchar(25) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `layout` varchar(50) NOT NULL,
  `controller` varchar(50) DEFAULT NULL,
  `roles` varchar(150) DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `type`, `url`, `description`, `keywords`, `layout`, `controller`, `roles`, `view_count`) VALUES
(1, 'Header', 'header', NULL, 'Header content', '', 'middle', NULL, NULL, 0),
(2, 'Footer', 'footer', NULL, 'Footer content', '', 'middle', NULL, NULL, 0),
(3, 'Home', 'home', '/', 'Home Page', 'shareads', 'middle', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `firstname`, `lastname`, `gender`) VALUES
(14, 'Phuc', 'Le', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL,
  `undeletable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `is_default`, `type`, `undeletable`) VALUES
(1, 'Admin', 'Administrator.', 0, 'admin', 1),
(2, 'User', 'Default user role.', 1, 'user', 1),
(3, 'Guest', 'Guest role.', 0, 'guest', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `name` varchar(60) NOT NULL,
  `value` varchar(250) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`name`, `value`) VALUES
('system_default_language', 'en'),
('system_theme', 'shareads'),
('system_title', 'Shareads');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_username` (`username`),
  KEY `ix_email` (`email`),
  KEY `fki-users-roles-role_id-id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `password`, `email`, `creation_date`, `modified_date`) VALUES
(1, 1, 'admin', '$2a$08$1CkG/RmxCq.LcyOm/Mw9xeauNflUXZ6oXWEFUDplbw64AncHHcWAO', 'quynhbkit@gmail.com', '2014-05-04 16:03:16', NULL),
(14, 2, 'phuc', '$2a$08$hFtRVoR4GGCPw0hkkbWsHO7D5kEARMpxRNpLdQQibWeCUxBQ99dnG', 'phuc@gmail.com', '2014-05-07 16:48:57', '2014-05-13 14:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `module` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_paginated` tinyint(1) NOT NULL DEFAULT '0',
  `is_acl_controlled` tinyint(1) NOT NULL DEFAULT '1',
  `admin_form` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `name`, `module`, `description`, `is_paginated`, `is_acl_controlled`, `admin_form`, `enabled`) VALUES
(1, 'HtmlBlock', 'core', 'Insert any HTML of you choice', 0, 1, 'action', 1),
(2, 'Menu', 'core', 'Render menu', 0, 1, '\\Core\\Form\\Admin\\Widget\\Menu', 1),
(3, 'Header', 'core', 'Settings for header of you site.', 0, 1, '\\Core\\Form\\Admin\\Widget\\Header', 1),
(4, 'Demowidget', 'membership', 'test', 0, 0, 'action', 1);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
