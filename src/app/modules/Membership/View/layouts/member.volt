<!DOCTYPE html>
<html>
<head>
    <title>{{ helper('setting', 'core').get('system_title', '') }} | {% block title %}{% endblock %}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="Shareads"/>
    <?php $this->assets->addCss('external/flatlab/css/bootstrap.min.css')?>
    <?php $this->assets->addCss('external/flatlab/css/bootstrap-reset.css')?>
    <?php $this->assets->addCss('external/flatlab/assets/font-awesome/css/font-awesome.css')?>
    <?php $this->assets->addCss('external/flatlab/css/style.css')?>
    <?php $this->assets->addCss('external/flatlab/css/style-responsive.css')?>
    <?php $this->assets->addCss('external/flatlab/assets/bootstrap-fileupload/bootstrap-fileupload.css')?>
    <?php $this->assets->addCss('external/autocomplete/content/styles.css')?>
    <?php $this->assets->addCss('assets/css/membership/main.css')?>

    {{ assets.outputCss() }}

    {{ assets.outputInline() }}

    <script type="text/javascript">
        {{ helper('i18n', 'core').render() }}
    </script>

    {%- block head -%}

    {%- endblock -%}

</head>
<body data-base-url="{{ url() }}" data-debug="{{ config.application.debug }}">
<!--header start-->
<header class="header white-bg">
    <div class="sidebar-toggle-box">
        <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
    </div>
    <!--logo start-->
    <a href="#" class="logo"><span>ShareAds</span>.us</a>
    <!--logo end-->
    <div class="nav notify-row" id="top_menu">
        <!--  notification goes here -->
    </div>
    <div class="top-nav ">
        <!--search & user info goes here-->
        {%- block topnav -%}
            <ul class="nav pull-right top-menu">
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="{{ url('external/flatlab/img/avatar1_small.jpg') }}">
                        <span class="username">{{ helper('user', 'user').current().username }}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li><a href="{{ url('logout') }}"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
        {%- endblock -%}
    </div>

</header>
<!--header end-->

<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu goes here-->
        {%- block sidebar -%}
        {%- endblock -%}
    </div>
</aside>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!--main content goes here-->
        <div class="system-container">
            {{ content() }}
            {{ flashSession.output() }}
        </div>
        {%- block content -%}
        {%- endblock -%}

    </section>
</section>
<!--main content end-->
{{ assets.outputJs() }}
{{ helper('profiler', 'core').render() }}
</body>
</html>