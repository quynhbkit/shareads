<!DOCTYPE html>
<html>
<head>
    <title>{{ helper('setting', 'core').get('system_title', '') }} | {% block title %}{% endblock %}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="Shareads"/>
    <?php $this->assets->addCss('external/bootstrap/css/bootstrap.min.css')?>
    <?php $this->assets->addCss('external/flatlab/css/bootstrap-reset.css')?>
    <?php $this->assets->addCss('external/flatlab/assets/font-awesome/css/font-awesome.css')?>
    <?php $this->assets->addCss('assets/css/membership/classic.css')?>
    <?php $this->assets->addJs('assets/js/membership/template.js')?>
    <?php $this->assets->addJs('external/bootbox.min.js')?>

    {{ assets.outputCss() }}
    {{ assets.outputInline() }}
    <script type="text/javascript">
        {{ helper('i18n', 'core').render() }}
    </script>
    <style type="text/css">
        {{ helper('template','membership').getCss() }}
    </style>
    {%- block head -%}
    {%- endblock -%}
</head>
<body data-base-url="{{ url() }}"
      data-debug="{{ config.application.debug }}"
      data-theme-url="{{ url(['for':'templates-classic']) }}"
      data-theme-update-url="{{ url(['for':'templates-classic-update']) }}" class="theme-classic">
<!--system menu start-->

<header class="system-nav">
    <div class="container">
        <div class="row">
            <div class="horizontal-menu navbar-collapse collapse">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Messages</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!--system menu end-->
<div id="wrapper">
<div class="wrapper-border">
    <div class="container-fluid">
        <!--header start-->
        <header class="row header">
            <div class="col-md-9">
                <a href="#" class="btn btn-default btn-sm btn-header" data-template-pic="header-background">Add header picture</a>

                <div class="avatar">
                    <img class="img-circle" src="http://placehold.it/140x140"/>
                    <div class="username vertical-align">BenHogan<br /><em class="job-title">Shareads founder</em></div>
                </div>
            </div>
            <div class="col-md-3 text-right top-gif">
                {#<img src="http://placehold.it/200x300" />#}
                <a href="#" class="btn btn-default btn-sm btn-topgif" data-template-pic="top-gif">Add top gif</a>
            </div>
        </header>
        <div class="row">
             <div data-template-pic="heart-line" class="col-md-12 text-center header-row-2" style="background:url('http://www.shareads.us/PersonalHomePage/heartlinesNEW/surferline.png')"></div>
        </div>
        <div class="row header-row-3">
            <div class="col-md-3 text-center">
                <img src="http://placehold.it/120x130">
            </div>
            <div class="col-md-6 text-center">
                <h3>California Dreamin</h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="http://placehold.it/120x130"/>
            </div>
        </div>
        <!--header end-->

        <!--main content start-->
        <section class="row">
            <!--main content goes here-->
            <div class="system-container">
                {{ content() }}
                {{ flashSession.output() }}
            </div>
            {%- block content -%}

            {%- endblock -%}
        </section>
        </div>
    </div>
    </div>
<!--main content end-->

{{ assets.outputJs() }}
{{ helper('profiler', 'core').render() }}
</body>
</html>