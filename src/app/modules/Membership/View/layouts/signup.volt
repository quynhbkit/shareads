<!DOCTYPE html>
<html>
<head>
    <title>{{ helper('setting', 'core').get('system_title', '') }} | {% block title %}{% endblock %}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="Shareads"/>

    {{ assets.outputCss() }}
    {{ assets.outputInline() }}
    <script type="text/javascript">
        {{ helper('i18n', 'core').render() }}
    </script>
    {%- block head -%}
    {%- endblock -%}
</head>
<body data-base-url="{{ url() }}" data-debug="{{ config.application.debug }}">
<div class="container">
    <!--main content goes here-->
    <div class="system-container">
        {{ content() }}
    </div>
    <div class="error-wrapper">
        {%- block content -%}
        {%- endblock -%}
    </div>
    <!--main content end-->
</div>
{{ assets.outputJs() }}
</body>
</html>