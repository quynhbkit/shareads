<!DOCTYPE html>
<html>
<head>
    <title>{{ helper('setting', 'core').get('system_title', '') }} | {% block title %}{% endblock %}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="Shareads"/>

    {{ assets.outputCss() }}
    {{ assets.outputInline() }}

    <script type="text/javascript">
        {{ helper('i18n', 'core').render() }}
    </script>

    {%- block head -%}

    {%- endblock -%}

</head>
<body data-base-url="{{ url() }}" data-debug="{{ config.application.debug }}" class="login-body">
<div class="system-container">
    {{ content() }}
</div>

<div class="container">
    {%- block content -%}
    {%- endblock -%}
</div>
{{ assets.outputJs() }}
{{ helper('profiler', 'core').render() }}
</body>
</html>