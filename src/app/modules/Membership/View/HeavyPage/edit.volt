{% extends "Membership/View/layouts/member.volt" %}
{% block sidebar %}
    {{ partial('Membership/View/Profile/menu') }}
{% endblock %}

{% block content %}
    <div class="row">
        <aside class="col-lg-12">
            {{ form.render() }}
        </aside>
    </div>
{% endblock %}