{% extends "Membership/View/layouts/member.volt" %}
{% block sidebar %}
    {{ partial('Membership/View/Profile/menu') }}
{% endblock %}

{% block content %}
    <div class="row">
        <aside class="col-lg-4">
            {{ form.render() }}
        </aside>
        <aside class="profile-nav col-lg-8">
            {#{ widget }#}
        </aside>
    </div>
{% endblock %}