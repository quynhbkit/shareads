{% extends "Membership/View/layouts/member.volt" %}
{% block sidebar %}
    {{ partial('Membership/View/Profile/menu') }}
{% endblock %}

{% block content %}
    <?php $this->assets->addJs('assets/js/membership/form.js')?>
    <?php $this->assets->addJs('external/bootbox.min.js')?>
    <?php $this->assets->addJs('assets/js/membership/music-jackpot.js')?>
    <div class="row">
        <div class="col-sm-12 col-lg-6">
            <form method="post" id="form1" data-form-list>
                <section class="panel">
                    <header class="panel-heading">
                        {{ 'My Music Jackpot' | i18n }}
                    </header>
                    <div class="panel-body">
                        <div class="clearfix">
                            <div class="btn-group">
                                {#<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">#}
                                    {#{{ 'Action' | i18n }} <span class="caret"></span></button>#}
                                {#<ul role="menu" class="dropdown-menu">#}
                                    {#<li><a data-form-action="delete" href="#" data-action="{{ url(["for":"music-jackpot-delete"])}}">{{ 'Delete' | i18n }}</a></li>#}
                                {#</ul>#}
                                <a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" type="button" data-form-action="delete" data-action="{{ url(["for":"music-jackpot-delete"])}}">
                                    {{ 'Delete' | i18n }} <i class="icon-trash"></i></a>
                            </div>
                            &nbsp;
                            <div class="btn-group">
                                <a class="btn  btn-danger green" href="{{ url(['for':'music-jackpot-add']) }}">
                                    {{ 'Add New' | i18n }} <i class="icon-plus"></i>
                                </a>
                            </div>
                        </div>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th width="20"><input type="checkbox" name="chkAll"/></th>
                                <th>#</th>
                                <th>{{ 'Contest' | i18n }}</th>
                                <th>{{ 'Entry Name' | i18n }}</th>
                                <th>{{ 'Entry Date' | i18n }}</th>
                                <th>{{ 'Cover' | i18n }}</th>
                                <th>{{ 'Action' | i18n }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for item in items %}
                                <tr>
                                    <td><input type="checkbox" value="{{ item.id }}" name="chk[]"/></td>
                                    <td>{{ item.id }}</td>
                                    <td>{{ item.title }}</td>
                                    <td>{{ item.name }}</td>
                                    <td>{{ item.entry_date }}</td>
                                    <td>{{ item.cover }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="{{ url(['for': 'music-jackpot-edit', 'id': item.id]) }}"><i class="icon-pencil"></i></a>
                                        <span class="btn btn-danger btn-xs"><i class="icon-youtube-play" data-preview="{{ url(['for':'music-jackpot-preview', 'id': item.id]) }}"></i></span>
                                    </td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </section>
            </form>
        </div>
        <div class="col-sm-12 col-lg-6">
            <div id="video-placeholder"></div>
        </div>
    </div>
{% endblock %}