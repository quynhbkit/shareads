{% extends "Membership/View/layouts/member.volt" %}
{% block sidebar %}
    {{ partial('Membership/View/Profile/menu') }}
{% endblock %}

{% block content %}
    <?php $this->assets->addJs('external/markdown/js/bootstrap-markdown.js')?>
    <div class="row">
        <?php $form->setTitle('Edit Music Jackpot') ?>
        {{ form.openTag() }}
        <aside class="col-lg-6">
            {{ form.render() }}
        </aside>
        <aside class="col-lg-6">
            <section class="panel">
                <header class="panel-heading">
                    {{ 'Categories' | i18n }}
                </header>
                <div class="panel-body">
                    <div class="dd">
                        <ul class="dd-list">
                            {% for tree in trees %}
                                {{ helper('treeCheckbox','membership').render(tree, treeOptions) }}
                            {% endfor %}
                        </ul>
                    </div>
                </div>
            </section>
        </aside>
        {{ form.closeTag() }}
    </div>
{% endblock %}