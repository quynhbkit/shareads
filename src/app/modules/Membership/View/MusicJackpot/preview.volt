<section class="panel">
    <header class="panel-heading">
        {{ 'Preview Music Jackpot Video' | i18n }}
    </header>
    <div class="panel-body center">
        {{ entity.embed_code }}
    </div>
</section>