{#
 +------------------------------------------------------------------------+
 | PhalconEye CMS                                                         |
 +------------------------------------------------------------------------+
 | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
 +------------------------------------------------------------------------+
 | This source file is subject to the New BSD License that is bundled     |
 | with this package in the file LICENSE.txt.                             |
 |                                                                        |
 | If you did not receive a copy of the license and are unable to         |
 | obtain it through the world-wide-web, please send an email             |
 | to license@phalconeye.com so we can send you a copy immediately.       |
 +------------------------------------------------------------------------+
#}
{% extends "Membership/View/layouts/member.volt" %}
{% block sidebar %}
    {{ partial('Membership/View/Profile/menu') }}
{% endblock %}

{% block content %}
    <div class="row" data-ng-app="sha.membership">
        <aside class="profile-nav col-lg-3">
            {{ partial('Membership/View/Profile/info', ['tab':'edit-profile']) }}
        </aside>
        <aside class="col-lg-9">
            {{ form.render() }}
        </aside>
    </div>
{% endblock %}