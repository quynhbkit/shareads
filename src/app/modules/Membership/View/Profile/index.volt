{% extends "Membership/View/layouts/member.volt" %}
{% block sidebar %}
    {{ partial('Membership/View/Profile/menu') }}
{% endblock %}

{% block content %}
    <div class="row" data-ng-app="sha.membership">
        <aside class="profile-nav col-lg-3">
            {{ partial('Membership/View/Profile/info', ['tab':'profile']) }}
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    {{ profile.about | nl2br | markdown}}
                </div>
                <div class="panel-body bio-graph-info">
                    <h1>{{ profile.display_name }}</h1>
                    <div class="row">
                        <div class="bio-row">
                            <p><span>{{ 'First Name' | i18n }} </span>: {{ profile.firstname }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Last Name' | i18n }} </span>: {{ profile.lastname }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Address' | i18n }} </span>: {{ profile.street }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Apartment #' | i18n }} </span>: {{ profile.apartment }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'City' | i18n }} </span>: {{ profile.getCityName() }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'State' | i18n }} </span>: {{ profile.getStateName() }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Country' | i18n }} </span>: {{ profile.getCountryName()}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Year of birth' | i18n }}</span>: {{ profile.yearofbirth }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Occupation' | i18n }} </span>: {{ profile.occupation }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Email' | i18n }} </span>: {{ profile.getEmail() }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Mobile' | i18n }} </span>: {{ profile.mobile }}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>{{ 'Phone' | i18n }} </span>: {{ profile.phone }}</p>
                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>
{% endblock %}