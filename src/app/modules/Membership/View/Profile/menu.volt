<div id="sidebar" class="nav-collapse " tabindex="5000" style="overflow: hidden; outline: none;">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
        <li class="sub-menu dcjq-parent-li">
            <a href="{{ url('profile') }}" class="active">
                <i class="icon-user"></i>
                <span>{{ 'Profile' | i18n }}</span>
            </a>
        </li>

        <li>
            <a href="#">
                <i class="icon-sitemap"></i>
                <span>{{ 'Membership Levels' | i18n }}</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="icon-dollar"></i>
                <span>{{ 'Payment history' | i18n }}</span>
            </a>
        </li>
        <li class="sub-menu dcjq-parent-li">
            <a href="#" class="dcjq-parent">
                <i class="icon-calendar"></i>
                <span>{{ 'ShareADs Partnership' | i18n }}</span>
            </a>
            <ul class="sub" style="display: block;">
                <li><a href="#">{{ 'Other Earnings (Jobs)' | i18n }}</a></li>
                <li><a href="#">{{ 'Cash Jackpots' | i18n }}</a></li>
                <li><a href="{{ url(['for': 'litepage-edit']) }}">{{ 'Edit Light Page' | i18n }}</a></li>
                <li><a href="{{ url(['for': 'heavypage-edit']) }}">{{ 'Edit Heavy Page' | i18n }}</a></li>
                <li><a href="{{ url(['for': 'picture-caption-edit']) }}">{{ 'Picture Caption' | i18n }}</a></li>
                <li><a href="{{ url(['for': 'music-jackpot-list']) }}">{{ 'Music Jackpots' | i18n }}</a></li>
            </ul>
        </li>
        <li>
            <a>Personal layouts</a>
            <ul>
                <li>Classic</li>
                <li>Blog</li>
                <li>Singer</li>
            </ul>
        </li>
    </ul>
    <!-- sidebar menu end-->
</div>