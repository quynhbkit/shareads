<script>
    var profile = {{ profile | json_encode  }};
</script>
<section class="panel" data-ng-controller="AvatarCtrl">
    <div class="user-heading round">
        <a href="#">
            <img class="avatar" ng-if="hasAvatar" ng-src="[[ avatarUrl ]]" alt="">
            <img ng-if="!hasAvatar" src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=no+image" alt="">
        </a>
        <a href="javascript:void(0)" class="delete-avatar" ng-if="hasAvatar" ng-click="deleteAvatar()" title="{{ 'Delete avatar' | i18n }}"> <i class="icon-trash"></i></a>
        <h1>{{ profile.getFullName() }}</h1>
        <p>{{ profile.getEmail() }}</p>
    </div>

    <ul class="nav nav-pills nav-stacked">
        {%- macro setActive(tab, item) %}
            {%- if (tab is defined) and (tab == item) -%}
                {{ 'class="active"' }}
            {%- endif -%}
        {%- endmacro %}
        <li {{ setActive(tab, 'profile') }}><a href="{{ url('profile') }}"> <i class="icon-user"></i> {{ 'Profile' | i18n }}</a></li>
        <li {{ setActive(tab, 'edit-profile') }}><a href="{{ url('profile/edit') }}"> <i class="icon-edit"></i> {{ 'Edit profile' | i18n }}</a></li>
        <li>
            <a class="btn-file" href="{{ url('profile/edit') }}"> <i class="icon-picture"></i> {{ 'Edit avatar' | i18n }}
                <input type="file" data-ng-file-select="onFileSelect($files)" />
            </a>
        </li>
        <li><a href="{{ url('changepwd') }}"> <i class="icon-key"></i> {{ 'Change password' | i18n }}</a></li>
        <li><a href="{{ url('activities') }}"> <i class="icon-calendar"></i> {{ 'Recent Activity' | i18n }} <span class="label label-danger pull-right r-activity">9</span></a></li>
    </ul>
</section>