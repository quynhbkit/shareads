{% extends "Membership/View/layouts/dialog.volt" %}
{% block body %}
    <div class="row">
        {% for item in paginator.items %}
        <div class="col-md-3">
            <a href="#" class="thumbnail">
                <img src="{{ item.getUrl() }}" alt="{{ item.getTitle() }}" title="{{ item.getTitle() }}" />
                <div class="actions">
                    <button data-toggle="button" class="btn btn-danger" data-select="{{ item.getId() }}">
                        <i class="icon-play-circle" title="Choose"></i>
                    </button>
                </div>
            </a>
        </div>
        {% endfor %}
    </div>
    <div class="text-center">
        {{ partial("partials/paginator") }}
    </div>
{% endblock %}