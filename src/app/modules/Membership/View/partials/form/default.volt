{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

{{ partial(form.getErrorsView(), ['form': form]) }}
{{ partial(form.getNoticesView(), ['form': form]) }}
<section class="panel">
    {% if form.getTitle() or form.getDescription() %}
    <header class="panel-heading">
        {{ form.getTitle() }}
    </header>
    {% endif %}
    <div class="panel-body">
        {{ form.openTag() }}
        {% for element in form.getAll() %}
            {{ partial(form.getElementView(), ['element': element]) }}
        {% endfor %}
        {% if form.useToken() %}
            <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}">
        {% endif %}
        {{ form.closeTag() }}
    </div>
</section>
