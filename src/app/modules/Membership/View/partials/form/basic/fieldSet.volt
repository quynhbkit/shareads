<fieldset {{ fieldSet.renderAttributes() }}>
    {% if fieldSet.hasLegend() %}
        <legend>{{ fieldSet.getLegend()|i18n }}</legend>
    {% endif %}

    {% for element in fieldSet.getAll() %}
        {{ partial(form.getElementView(), ['element': element, 'combined': fieldSet.isCombined()]) }}
    {% endfor %}
</fieldset>