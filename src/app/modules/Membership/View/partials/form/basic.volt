{{ partial(form.getErrorsView(), ['form': form]) }}
{{ partial(form.getNoticesView(), ['form': form]) }}
<section class="panel">
    {% if form.getTitle() or form.getDescription() %}
    <header class="panel-heading">
        {{ form.getTitle() }}
    </header>
    {% endif %}
    <div class="panel-body">
        <?php $form->setAttribute('class','')?>
        {{ form.openTag() }}
        {% for element in form.getAll() %}
            {{ partial(form.getElementView(), ['element': element]) }}
        {% endfor %}
        {% if form.useToken() %}
            <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}">
        {% endif %}
        {{ form.closeTag() }}
    </div>
</section>
