{% extends "Core/View/layouts/admin.volt" %}


{% block title %}{{ 'Template pictures'|i18n }}{% endblock %}

{% block head %}
    <script type="text/javascript">
        var deleteItem = function (id) {
            if (confirm('{{ "Are you really want to delete this user?" |i18n}}')) {
                window.location.href = '{{ url(['for':'admin-template-picture-delete'])}}' + id;
            }
        }
    </script>
{% endblock %}

{% block header %}
    <div class="navbar navbar-header">
        <div class="navbar-inner">
            {{ navigation.render() }}
        </div>
    </div>
{% endblock %}

{% block content %}
    <div class="span12">
        <div class="row-fluid">
            <h2>{{ 'Template pictures' |i18n }} ({{ grid.getTotalCount() }})</h2>
            {{ grid.render() }}
        </div>
    </div>
{% endblock %}