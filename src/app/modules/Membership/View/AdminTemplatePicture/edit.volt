{% extends "Core/View/layouts/admin.volt" %}
{% block title %}{{ "Template picture Editing"|i18n }}{% endblock %}

{% block header %}
    <div class="navbar navbar-header">
        <div class="navbar-inner">
            {{ navigation.render() }}
        </div>
    </div>
{% endblock %}

{% block content %}
    <div class="span12">
        <div class="row-fluid">
            {{ form.render() }}
        </div>
        <!--/row -->
    </div><!--/span-->

{% endblock %}
