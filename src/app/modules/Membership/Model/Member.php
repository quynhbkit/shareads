<?php
namespace Membership\Model;

use User\Model\User as CoreUser;
use Phalcon\DI;

/**
 * Class Member
 * @package Membership\Model
 *
 * @Source('users')
 *
 * HasOne("id",'Membership\Model\Profile','user_id', {alias: "profile"})
 * HasOne("id",'Membership\Model\LitePage','user_id', {alias: "litepage"})
 * HasOne("id",'Membership\Model\HeavyPage','user_id', {alias: "heavypage"})
 * HasMany("id",'Membership\Model\Album','user_id', {alias: "albums"})
 *
 * @Acl(actions=[register,index])
 */
class Member extends CoreUser implements MemberInterface{

    /**
     * Current viewer.
     *
     * @var User null
     */
    private static $_viewer = null;

    public function initialize()
    {
        $this->hasOne("id", "Membership\Model\Profile", "user_id", array(
            'alias' => 'profile'
        ));

        $this->hasOne("id", "Membership\Model\LitePage", "user_id", array(
            'alias' => 'litepage'
        ));

        $this->hasOne("id", "Membership\Model\HeavyPage", "user_id", array(
            'alias' => 'heavypage'
        ));
    }

    public function getEmail(){
        return $this->email;
    }

    /**
     * Get current user
     * If user logged in this function will return user object with data
     * If user isn't logged in this function will return empty user object with ID = 0
     *
     * @return Member
     */
    public static function getViewer()
    {
        if (null === self::$_viewer) {
            $identity = DI::getDefault()->get('core')->auth()->getIdentity();
            if ($identity) {
                self::$_viewer = self::findFirst($identity);
            }
        }

        return self::$_viewer;
    }

    /**
     * Return the related "Album" entity.
     *
     * @param array $arguments Entity arguments.
     *
     * @return Album[]
     */
    public function getAlbums($arguments=[]){
        return $this->getRelated('albums', $arguments);
    }
} 