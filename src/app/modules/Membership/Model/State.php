<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
/**
 * Class State
 * @package Membership\Model
 *
 * @Source("sha_states")
 */
class State extends AbstractModel{
    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;


    /**
     * @Column(type="integer", nullable=false, column="country_id", size="11")
     */
    public $country_id;

    /**
     * @Column(type="string", nullable=false, column="name", size="100")
     */
    public $name;


    public function initialize()
    {
        $this->belongsTo("country_id", "Membership\Model\Country", "id", array(
            'alias' => 'country'
        ));
        $this->hasMany("id", "Membership\Model\City", "state_id", array(
            'alias' => 'cities'
        ));
    }
} 