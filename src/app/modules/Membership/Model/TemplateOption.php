<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;
use Phalcon\DI\Injectable;

/**
 * Class Template
 * @package Membership\Model
 */
class TemplateOption extends Injectable{
    static public function getTemplateOptions($userId){
        $data = [];
        $items = UserTemplate::getTemplateParams($userId);
        foreach($items as $item){
            $data[$item->type][]=[];
        }
    }
} 