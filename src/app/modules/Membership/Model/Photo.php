<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
use Engine\Db\Model\Behavior\Timestampable;

/**
 * Class Album
 * @package Membership\Model
 *
 * @Source("sha_photos")
 * @Acl(actions=[edit,delete])
 */
class Photo extends AbstractModel implements PhotoInterface
{
    use Timestampable;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;

    /**
     * @Column(type="integer", nullable=true, column="user_id", size="11")
     */
    public $user_id;

    /**
     * @Column(type="integer", nullable=true, column="album_id", size="11")
     */
    public $album_id;

    /**
     * @Column(type="string", nullable=false, column="filename", size="100")
     */
    public $filename;

    /**
     * @Column(type="string", nullable=true, column="caption", size="254")
     */
    public $caption;

    /**
     * @Column(type="string", nullable=true, column="description", size="254")
     */
    public $description;

    /**
     * @Column(type="integer", nullable=false, column="status", size="2")
     */
    public $status;

    public function initialize()
    {
        $this->belongsTo("album_id", "Membership\Model\Album", "id", array(
            'alias' => 'album'
        ));
        $this->belongsTo("user_id", "Membership\Model\Member", "id", array(
            'alias' => 'user'
        ));
    }

    protected function beforeValidation()
    {
        if (empty($this->status)) {
            $this->status = 0;
        }
    }

    protected function afterDelete()
    {
        $di = $this->getDI();
        $path = $di->get('membership')->member()->getPhotoFolder() . $this->album_id . '/' . $this->filename;
        @unlink($path);
    }

    /**
     * Set Album
     *
     * @param AlbumInterface $album
     */
    public function setAlbum(AlbumInterface $album)
    {
        $this->album = $album;
    }


    /**
     * Get photo URL
     * @return string
     */
    public function getUrl()
    {
        $di = $this->getDI();
        $path = $di->get('membership')->member()->getPhotoFolder() . $this->album_id . '/' . $this->filename;
        return $di->get('url')->get($path);
    }
} 