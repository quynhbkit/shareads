<?php
namespace Membership\Model;
use Engine\Db\AbstractModel;

/**
 * Class HeavyPage
 * @package Membership\Model
 *
 * @Source("sha_heavypages")
 *
 * @Acl(actions=["edit"])
 */
class HeavyPage extends AbstractModel {

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    protected $id;

    /**
     * @Column(type="integer", nullable=false, column="user_id", size="11")
     */
    protected $user_id;

    /**
     * @Column(type="string", nullable=true, column="content")
     */
    protected $content;

    public function initialize()
    {
        $this->hasOne("user_id", "\Membership\Model\Member", "id", array(
            'alias' => 'user'
        ));
    }
    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }
} 