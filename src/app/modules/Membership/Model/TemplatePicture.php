<?php
namespace Membership\Model;
use Engine\Db\AbstractModel;
use cbednarski\FileUtils\FileUtils;
use Membership\Helper\File as FileHelper;
use Membership\Helper\String as StringHelper;

/**
 * Class Template Options
 * @package Membership\Model
 *
 * @Source("sha_template_pictures")
 * @Acl(actions=[classic,update])
 */
class TemplatePicture extends AbstractModel {

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    protected $id;

    /**
     * @Column(type="string", nullable=true, column="title")
     */
    protected $title;

    /**
     * @Column(type="integer", nullable=false, column="term_id", size="11")
     */
    protected $term_id;

    /**
     * @Column(type="string", nullable=true, column="filename")
     */
    protected $filename;

    /**
     * @Column(type="string", nullable=true, column="description")
     */
    protected $description;


    public function initialize(){
        $this->belongsTo('term_id','Membership\Model\Term', 'id',['alias'=>'Term']);
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $term_id
     */
    public function setTermId($term_id)
    {
        $this->term_id = $term_id;
    }

    /**
     * @return mixed
     */
    public function getTermId()
    {
        return $this->term_id;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Return the related "Term" entity.
     *
     * @param array $arguments Arguments data.
     *
     * @return Term
     */
    public function getTerm($arguments = [])
    {
        $term =  $this->getRelated('Term', $arguments);
        if(!$term && $this->getTermId()){
            $term =  Term::findFirstById($this->getTermId());
        }
        return $term;
    }

    public function getDir(){
        $term = $this->getTerm();
        $path = PUBLIC_PATH.'/files/res/'.$term->getTaxonomy().'/'.$term->getSlug();

        FileUtils::mkdirIfNotExists($path);
        return $path;
    }

    public function normalizeFileName($filename){
        $name = FileHelper::getNameWithoutExt($filename);
        $ext = FileHelper::getExt($filename);
        $name = StringHelper::generateSlug($name);
        if($ext){
            $name.='.'.$ext;
        }
        return $name;
    }

    public function getFilePath($filename=null){
        $dir = $this->getDir();
        if(!$filename){
            $filename = $this->getFilename();
        }else{
            $filename = $this->normalizeFileName($filename);
            $filename = FileHelper::generateFileName($dir, $filename);
        }
        return rtrim($dir,'/').'/'.$filename;
    }

    public function getUrl(){
        $term = $this->getTerm();
        $url = 'files/res/'.$term->getTaxonomy().'/'.$term->getSlug().'/'.$this->getFilename();
        return $this->getDI()->get('url')->getStatic($url);
    }

    public function beforeUpdate(){
        $old = self::findFirst($this->getId());
        if($this->getFilename() !=$old->getFilename()){
            @unlink($old->getFilePath());
        }
    }

    public function afterDelete(){
        $path = $this->getFilePath();
        if(file_exists($path)){
            @unlink($path);
        }
    }
} 