<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
/**
 * Class City
 * @package Membership\Model
 *
 * @Source("sha_cities")
 */
class City extends AbstractModel{
    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;


    /**
     * @Column(type="integer", nullable=false, column="country_id", size="11")
     */
    public $country_id;

    /**
     * @Column(type="integer", nullable=false, column="state_id", size="11")
     */
    public $state_id;

    /**
     * @Column(type="string", nullable=false, column="name", size="100")
     */
    public $name;


    public function initialize()
    {
        $this->belongsTo("country_id", "Membership\Model\Country", "id", array(
            'alias' => 'country'
        ));
        $this->belongsTo("state_id", "Membership\Model\State", "id", array(
            'alias' => 'state'
        ));
    }
} 