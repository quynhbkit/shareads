<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
/**
 * Class Country
 * @package Membership\Model
 *
 * @Source("sha_countries")
 */
class Country extends AbstractModel{
    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;

    /**
     * @Column(type="string", nullable=false, column="name", size="100")
     */
    public $name;

    public function initialize()
    {
        $this->hasMany("id", "Membership\Model\City", "country_id", array(
            'alias' => 'cities'
        ));
        $this->hasMany("id", "Membership\Model\State", "country_id", array(
            'alias' => 'states'
        ));
    }
} 