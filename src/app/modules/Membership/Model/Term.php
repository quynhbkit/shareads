<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
use Engine\Db\Model\Behavior\Timestampable;
use Membership\Behavior\TreeBehavior;

/**
 * Class Term
 * @package Membership\Model
 *
 * @Source("sha_terms")
 * @BelongsTo("id","\Membership\Model\Term", "parent_id", {alias: "parent"})
 * @HasMany("parent_id","\Membership\Model\Term", "id", {alias: "children"})
 */
class Term extends AbstractModel implements TermInterface
{
    use Timestampable;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    protected $id;

    /**
     * @Column(type="integer", nullable=true, column="parent_id", size="11")
     */
    protected $parent_id;

    /**
     * @Column(type="string", nullable=false, column="name", size="255")
     */
    protected $name;

    /**
     * @Column(type="string", nullable=false, column="alias", size="255")
     */
    protected $slug;

    /**
     * @Column(type="string", nullable=false, column="alias", size="100")
     */
    protected $taxonomy;

    /**
     * @Column(type="integer", nullable=true, column="lft", size="11")
     */
    protected $lft;
    /**
     * @Column(type="integer", nullable=true, column="rght", size="11")
     */
    protected $rght;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $modified_date
     */
    public function setModifiedDate($modified_date)
    {
        $this->modified_date = $modified_date;
    }

    /**
     * @return mixed
     */
    public function getModifiedDate()
    {
        return $this->modified_date;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param mixed $rght
     */
    public function setRght($rght)
    {
        $this->rght = $rght;
    }

    /**
     * @return mixed
     */
    public function getRght()
    {
        return $this->rght;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $taxonomy
     */
    public function setTaxonomy($taxonomy)
    {
        $this->taxonomy = $taxonomy;
    }

    /**
     * @return mixed
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id',
            'name' => 'name',
            'slug' => 'slug',
            'taxonomy' => 'taxonomy',
            'parent_id' => 'parentId',
            'lft' => 'lft',
            'rght' => 'rght',
            'creation_date' => 'creationDate',
            'modified_date' => 'modifiedDate'
        );
    }

    public function initialize(){
        $this->addBehavior(new TreeBehavior());
    }


    /**
     * Return the related "Children" entity.
     *
     * @param array $arguments Entity arguments.
     *
     * @return Term[]
     */
    public function getChildren($arguments = [])
    {
        return $this->getRelated('children', $arguments);
    }

    /**
     * Return the related "Parent" model.
     *
     * @param array $arguments Model arguments.
     *
     * @return Term
     */
    public function getParent($arguments = [])
    {
        return $this->getRelated('parent', $arguments);
    }
}