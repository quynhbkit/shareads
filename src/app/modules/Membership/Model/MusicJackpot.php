<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
use Engine\Db\Model\Behavior\Sluggable;
use Engine\Db\Model\Behavior\Timestampable;

/**
 * Class MusicJackpot
 * @package Membership\Model
 *
 * @Source("sha_music_jackpots")
 * @BelongsTo("user_id","\Membership\Model\Member", "id", {alias: "member"})
 * @HasMany("id","\Membership\Model\TermRelationship", "object_id", {alias: "TermRelationships"})
 *
 * @Acl(actions=[add,edit,list,delete,preview,view])
 */
class MusicJackpot extends AbstractModel implements MusicJackpotInterface
{
    use Timestampable;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;


    /**
     * @Column(type="integer", nullable=true, column="user_id", size="11")
     */
    public $user_id;

    /**
     * @Column(type="string", nullable=false, column="title", size="255")
     */
    public $title;

    /**
     * @Column(type="string", nullable=false, column="name", size="100")
     */
    public $name;

    /**
     * @Column(type="string", nullable=true, column="entry_date")
     */
    public $entry_date;

    /**
     * @Column(type="string", nullable=false, column="cover", size="255")
     */
    public $cover;

    /**
     * @Column(type="string", nullable=false, column="url", size="255")
     */
    public $url;

    /**
     * @Column(type="string", nullable=true, column="embed_code", size="1000")
     */
    public $embed_code;

    /**
     * @Column(type="string", nullable=true, column="owner", size="100")
     */
    public $owner;

    /**
     * @Column(type="string", nullable=true, column="connection", size="255")
     */
    public $connection;

    /**
     * @Column(type="string", nullable=true, column="improvement", size="255")
     */
    public $involvement;

    /**
     * @Column(type="string", nullable=true, column="improvement", size="4000")
     */
    public $about;

    /**
     * Return the related "User" model.
     *
     * @param array $arguments Model arguments.
     *
     * @return MemberInterface
     */
    public function getUser($arguments = [])
    {
        return $this->getRelated('member', $arguments);
    }

    /**
     * Return the related "TermRelationship" model.
     *
     * @param array $arguments Model arguments.
     *
     * @return TermRelationship[]
     */
    public function getTermRelationships($arguments = [])
    {
        return $this->getRelated('TermRelationships', $arguments);
    }


    /**
     * Find model by ID
     *
     * @param $id ID
     * @param null $userId User ID
     * @return PhalconModel
     */
    public static function findById($id, $userId = null)
    {
        if ($userId) {
            return self::findFirst(['id =?0 AND user_id = ?1', 'bind' => [$id, $userId]]);
        }
        return self::findFirst($id);
    }

    public function beforeDelete(){
        foreach($this->getTermRelationships() as $term){
            if(!$term->delete()){
                return false;
            }
        }
        return true;
    }
} 