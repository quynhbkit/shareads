<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
use Engine\Db\Model\Behavior\Timestampable;
use Engine\Db\Model\Behavior\Sluggable;

/**
 * Class Album
 * @package Membership\Model
 *
 * @Source("sha_albums")
 * @BelongsTo("user_id","\Membership\Model\Member", "id", {alias: "member"})
 * @HasMany("id","\Membership\Model\Photo", "album_id", {alias: "photos"})
 */
class Album extends AbstractModel implements AlbumInterface
{
    // use Timestampable;
    use Sluggable;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;


    /**
     * @Column(type="integer", nullable=true, column="user_id", size="11")
     */
    public $user_id;

    /**
     * @Column(type="string", nullable=false, column="title", size="100")
     */
    public $title;

    /**
     * Return the related "Photo" entity.
     *
     * @param array $arguments Entity arguments.
     *
     * @return Photo[]
     */
    public function getPhotos($arguments = [])
    {
        return $this->getRelated('photos', $arguments);
    }

    /**
     * Return the related "User" model.
     *
     * @param array $arguments Model arguments.
     *
     * @return Member
     */
    public function getUser($arguments = [])
    {
        return $this->getRelated('member', $arguments);
    }

    /**
     * Get picture caption album
     *
     * @param $userId
     * @return AlbumInterface|null
     */
    public static function getPictureCaptionAlbum($userId)
    {
        $slug = 'picture-caption';
        $album = self::findFirst(array('user_id =?0 AND slug = ?1', 'bind' => [$userId, $slug]));
        if (!$album) {
            $album = new Album();
            $album->slug = $slug;
            $album->user_id = $userId;
            $album->name = 'Picture Caption';

            if (!$album->save()) {
                return null;
            }
        }
        return $album;
    }
} 