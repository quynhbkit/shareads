<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
use Engine\Db\Model\Behavior\Timestampable;
use Membership\Behavior\TreeBehavior;

/**
 * Class Term
 * @package Membership\Model
 *
 * @Source("sha_term_relationships")
 * @BelongsTo("term_id","\Membership\Model\Term", "id", {alias: "term"})
 */
class TermRelationship extends AbstractModel
{
    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    protected $id;

    /**
     * @Column(type="integer", nullable=false, column="term_id", size="11")
     */
    protected $term_id;

    /**
     * @Column(type="integer", nullable=false, column="object_id", size="11")
     */
    protected $object_id;

    /**
     * @Column(type="integer", nullable=true, column="order", size="11")
     */
    protected $order;

    public function initialize(){

    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $object_id
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;
    }

    /**
     * @return mixed
     */
    public function getObjectId()
    {
        return $this->object_id;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $term_id
     */
    public function setTermId($term_id)
    {
        $this->term_id = $term_id;
    }

    /**
     * @return mixed
     */
    public function getTermId()
    {
        return $this->term_id;
    }




    /**
     * Return the related "Term" entity.
     *
     * @param array $arguments Entity arguments.
     *
     * @return Term
     */
    public function getTerm($arguments = [])
    {
        return $this->getRelated('term', $arguments);
    }
}