<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Model;

use Engine\Db\AbstractModel;
/**
 * Class User Template
 * @package Membership\Model
 *
 * @Source("sha_user_templates")
 */
class UserTemplate extends AbstractModel{
    const PICTURE = 'picture';
    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;


    /**
     * @Column(type="integer", nullable=true, column="user_id", size="11")
     */
    public $user_id;

    /**
     * @Column(type="string", nullable=false, column="param", size="100")
     */
    public $param;

    /**
     * @Column(type="string", nullable=false, column="data")
     */
    public $data;

    /**
     * @Column(type="string", nullable=false, column="type")
     */
    public $type;


    public function initialize()
    {
        $this->belongsTo("user_id", "User\Model\User", "id", array(
            'alias' => 'User'
        ));
    }


    /**
     * Return the related "User" entity.
     *
     * @param array $arguments Arguments data.
     *
     * @return User
     */
    public function getUser($arguments = [])
    {
        return $this->getRelated('User', $arguments);
    }

    static public function getTemplateParam($userId, $param){
        return self::findFirst(['user_id = ?0 AND param = ?1', 'bind'=>[$userId, $param]]);
    }

    static public function getTemplateParams($userId){
        return self::find(['user_id = ?0', 'bind'=>[$userId]]);
    }

    public function getPictureUrl(){
        if($this->type!==self::PICTURE){
            return null;
        }
    }
} 