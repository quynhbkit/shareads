<?php
namespace Membership\Model;

use Engine\Db\AbstractModel;
use Membership\Helper\File;

/**
 * Class Profile
 * @package Membership\Model
 *
 * @Source("sha_profiles")
 * @Acl(actions=[index, edit, upload, avatar])
 */
class Profile extends AbstractModel
{
    const AVATAR_DIR = 'files/avatars/';
    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="user_id", size="11")
     */
    public $user_id;

    /**
     * @Column(type="integer", nullable=false, column="gender", size="4")
     */
    public $gender;

    /**
     * @Column(type="string", nullable=false, column="firstname", size="60")
     */
    public $firstname;

    /**
     * @Column(type="string", nullable=true, column="lastname", size="60")
     */
    public $lastname;

    /**
     * @Column(type="string", nullable=true, column="display_name", size="60")
     */
    public $display_name;

    /**
     * @Column(type="string", nullable=true, column="lastname", size="60")
     */
    public $zipcode;

    /**
     * @Column(type="string", nullable=true, column="street", size="254")
     */
    public $street;

    /**
     * @Column(type="integer", nullable=true, column="country_id", size="11")
     */
    public $country_id;

    /**
     * @Column(type="integer", nullable=true, column="city_id", size="11")
     */
    public $city_id;

    /**
     * @Column(type="integer", nullable=true, column="state_id", size="11")
     */
    public $state_id;

    /**
     * @Column(type="integer", nullable=true, column="yearofbirth", size="4")
     */
    public $yearofbirth;

    /**
     * @Column(type="string", nullable=true, column="avatar", size="100")
     */
    public $avatar;

    /**
     * @Column(type="string", nullable=true, column="apartment", size="100")
     */
    public $apartment;

    /**
     * @Column(type="string", nullable=true, column="mobile", size="50")
     */
    public $mobile;

    /**
     * @Column(type="string", nullable=true, column="phone", size="50")
     */
    public $phone;

    /**
     * @Column(type="string", nullable=true, column="occupation", size="200")
     */
    public $occupation;

    /**
     * @Column(type="string", nullable=true, column="about", size="1000")
     */
    public $about;


    public function initialize()
    {
        $this->hasOne("country_id", "Membership\Model\Country", "id", array(
            'alias' => 'country'
        ));
        $this->hasOne("city_id", "Membership\Model\City", "id", array(
            'alias' => 'city'
        ));

        $this->hasOne("state_id", "Membership\Model\State", "id", array(
            'alias' => 'state'
        ));

        $this->hasOne("user_id", "Membership\Model\Member", "id", array(
            'alias' => 'user'
        ));
    }

    public function updateAvatar($filename, $base64)
    {

        $img = str_replace('data:image/png;base64,', '', $base64);
        $img = str_replace(' ', '+', $img);
        if (!$img) {
            return false;
        }
        $oldFile = $this->avatar;
        $data = base64_decode($img);
        $filename = File::generateFileName(self::AVATAR_DIR, $filename);
        $filename = preg_replace("/^(.*?)(\.[a-z]{3,4})$/i", "$1", $filename) . '.png';
        $path = self::AVATAR_DIR . $filename;
        if (file_put_contents($path, $data)) {
            $this->avatar = $filename;
            $this->save();
            if ($oldFile) {
                @unlink(self::AVATAR_DIR . $oldFile);
            }
        }
        return true;
    }

    public function deleteAvatar()
    {
        $oldFile = $this->avatar;
        if ($oldFile) {
            @unlink(self::AVATAR_DIR . $oldFile);
            $this->avatar = null;
            $this->save();
        }
        return true;
    }

    public function getAvatarUrl()
    {
        if(!$this->avatar){
            return '';
        }
        $url = $this->getDI()->get('url');
        if(!$url){
            return $this->avatar;
        }
        return $url->get(self::AVATAR_DIR.$this->avatar);
    }

    /**
     * Get fullname
     *
     * @todo format of fullname should be configurable
     * somewhere (ex: %F %L)
     *
     * @return string
     */
    public function getFullName(){
        return sprintf('%s %s', $this->firstname, $this->lastname);
    }

    /**
     * Get address
     *
     * @todo format of address should be configurable
     * somewhere (ex: %C %S %C)
     *
     * @return string
     */
    public function getAddress(){
        return sprintf('%s, %s, %s', $this->getCityName(), $this->getStateName(), $this->getCountryName());
    }

    public function getEmail(){
        return $this->user->getEmail();
    }

    public function getCityName(){
        if(($city= $this->city)){
            return $city->name;
        }
        return '';
    }
    public function getStateName(){
        if(($state= $this->state)){
            return $state->name;
        }
        return '';
    }
    public function getCountryName(){
        if(($country= $this->country)){
            return $country->name;
        }
        return '';
    }
} 