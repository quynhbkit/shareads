<?php
namespace Membership\Model;

use Phalcon\Mvc\Model\Query;
use Engine\Behaviour\DIBehaviour;
use Phalcon\DI;

/**
 * Class Data
 * @package Membership\Model
 *
 * @Acl(actions=["country","city","state"])
 */
class Data{
    use DIBehaviour;

    public static function getCountries($q)
    {
        $di = DI::getDefault();
        $query = new Query('SELECT * FROM Membership\Model\Country WHERE name LIKE :name: LIMIT 20', $di);
        $rows = $query->execute(array('name'=>$q.'%'));
        $items =[];
        foreach($rows as $row){
            $items[]=$row;
        }
        return $items;
    }

    public static function getCities($q)
    {
        $di = DI::getDefault();
        $query = new Query('SELECT * FROM Membership\Model\City WHERE name LIKE :name: LIMIT 20', $di);
        $rows = $query->execute(array('name'=>$q.'%'));
        $items =[];
        foreach($rows as $row){
            $row->state;
            $row->country;
            $items[]=$row;
        }
        return $items;
    }

    public static function getStates($q)
    {
        $di = DI::getDefault();
        $query = new Query('SELECT * FROM Membership\Model\State WHERE name LIKE :name: LIMIT 20', $di);
        $rows = $query->execute(array('name'=>$q.'%'));
        $items =[];
        foreach($rows as $row){
            $row->country;
            $items[]=$row;
        }
        return $items;
    }
} 