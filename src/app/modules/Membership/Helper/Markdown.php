<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 6/20/14
 * Time: 10:29 PM
 */

namespace Membership\Helper;

use \Michelf\Markdown as MarkdownTransform;


class Markdown extends \Phalcon\DI\Injectable
{
    public static function filter($value)
    {
        return MarkdownTransform::defaultTransform($value);
    }
} 