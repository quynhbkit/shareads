<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 7/27/14
 * Time: 12:18 PM
 */

namespace Membership\Helper;


class TreeCheckbox implements TreeRendererInterface
{
    public function render($tree, $options=array())
    {
        return $this->_render($tree, $options);
    }

    protected function _render($node, $options)
    {
        $values = isset($options['values'])?(array)$options['values']:array();
        $value = $node['id'];
        $checked = in_array($value, $values)?'checked':'';

        $html = '<li class="dd-item"><div>'.
        '<input name="term_id[]" type="checkbox" value="'.$node['id'].'" '.$checked.' /> ' . $node['name'].'</div>';
        if (!isset($node['children']) || !count($node['children'])) {
            $html .= '</li>';
        } else {
            $html .= '<ul class="dd-list">';
            foreach ($node['children'] as $child) {
                $html .= $this->_render($child, $options);
            }
            $html .= '</ul>';
        }

        return $html;
    }
} 