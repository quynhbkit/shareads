<?php

namespace Membership\Helper;


class File
{
    public static function  generateFileName($dir, $filename)
    {
        $dir =  rtrim($dir,'/').'/';
        $path =$dir.$filename;
        $actualName = pathinfo($path, PATHINFO_FILENAME);
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $originalName = $actualName;

        $i = 0;
        while (file_exists($path)) {
            $i++;
            $actualName = (string)$originalName .'-'. $i;
            $path = $dir.$actualName . "." . $extension;
        }
        return $actualName.'.'.$extension;
    }

    static public function getFileSize($filePath, $digits = 2)
    {
        if (is_file($filePath)) {
            $fileSize = filesize($filePath);
            return self::formatFileSize($fileSize, $digits);
        }
        return null;
    }

    static public function formatFileSize($fileSize, $digits = 2)
    {
        $sizes = array("TB", "GB", "MB", "KB", "B");
        $total = count($sizes);
        while ($total-- && $fileSize > 1024) {
            $fileSize /= 1024;
        }
        return round($fileSize, $digits) . " " . $sizes[$total];
    }

    static public function getNameWithoutExt($filename){
        $pattern = "/(.*?)(\.[a-zA-Z0-9]{3,4})$/";
        return preg_replace($pattern,"$1", $filename);
    }

    static public function getExt($filename){
        $pattern = "/(.*?)(\.)([a-zA-Z0-9]{3,4})$/";
        return preg_replace($pattern,"$3", $filename);
    }
} 