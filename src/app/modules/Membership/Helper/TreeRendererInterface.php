<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 7/27/14
 * Time: 12:17 PM
 */

namespace Membership\Helper;


interface TreeRendererInterface {
    public function render($tree, $options=array());
} 