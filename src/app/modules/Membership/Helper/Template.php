<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 5/27/14
 * Time: 8:51 PM
 */

namespace Membership\Helper;
use Engine\Helper;
use User\Model\User;
use Membership\Model\UserTemplate;
use Membership\Model\TemplatePicture;

/**
 * Class Template
 * @package Membership\Model
 */
class Template extends Helper{
    static public function getCss(){
        $userId = User::getViewer()->getId();
        $css = '';
        $item = UserTemplate::getTemplateParam($userId, 'header-background');
        if($item){
            $pic = TemplatePicture::findFirst($item->data);
            $css .= '.theme-classic
                .header{
                    background: url("'.$pic->getUrl().'") no-repeat
                }
            ';
        }
        $item = UserTemplate::getTemplateParam($userId, 'top-gif');
        if($item){
            $pic = TemplatePicture::findFirst($item->data);
            $css .= '.theme-classic
                .header .top-gif{
                    background: url("'.$pic->getUrl().'");
                    background-repeat: no-repeat;
                    background-size: cover
                }
            ';
        }

        return $css;
    }
} 