<?php
namespace Membership\Command;


use Engine\Console\AbstractCommand;
use Engine\Console\CommandInterface;
use Engine\Console\ConsoleUtil;
use Membership\Model\City;
use Membership\Model\Country;
use Membership\Model\State;


/**
 * To define sub command - add subcommandAction method. It will be automatically added as sub command.
 *
 * @CommandName(['import'])
 * @CommandDescription('import master data command.')
 */
class Import extends AbstractCommand implements CommandInterface{
    protected function _scanFiles($dir){
        $txtFiles = [];

        // Get all file names.
        $files = scandir($dir);
        // Iterate files.
        foreach ($files as $file) {
            if ($file == "." || $file == "..") {
                continue;
            }
            $txtFiles[]=$file;
        }

        return $txtFiles;
    }

    public function cityAction(){
        $dir = '/var/www/html/shareads/_db/cities/USA/';
        $files = $this->_scanFiles($dir);
        $file = $dir .$files[0];
        $fp = fopen($file,'r');
        while(($data =fgetcsv($fp))){
            $city = $data[0];
            if(count($data)>=3){
                $state = trim($data[1]);
                $country = trim($data[2]);
            }else{
                $state = $city;
                $country = trim($data[1]);
            }

            $status = $this->_importCity($city, $state, $country);
            if($status){
                print ConsoleUtil::success ('Done '.$city). PHP_EOL;
            }else{
                print ConsoleUtil::error('Failed '.$city). PHP_EOL;
            }
        }
        fclose($fp);
        print ConsoleUtil::success ('imported cities successfully'). PHP_EOL;
    }

    protected function _importCity($cityName, $stateName, $countryName){
        $country = Country::findFirst(['conditions'=>'name=?1', 'bind'=>[1=>$countryName]]);
        if(!$country){
            $country = new Country();
            $country->name = $countryName;
        }

        if(!($state = State::findFirst(['conditions'=>'name=?1', 'bind'=>[1=>$stateName]]))){
            $state = new State();
            $state->name = $stateName;
        }
        $country->save();
        $state->country_id=$country->id;
        $state->save();

        $city = new City();
        $city->name = $cityName;
        $city->country_id = $country->id;
        $city->state_id = $state->id;

        return $city->save();
    }

    public function stateAction(){
        print ConsoleUtil::success('Assets successfully installed.') . PHP_EOL;
    }
}