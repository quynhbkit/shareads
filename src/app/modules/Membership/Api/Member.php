<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 7/9/14
 * Time: 9:27 PM
 */

namespace Membership\Api;

use cbednarski\FileUtils\FileUtils;
use Engine\Api\AbstractApi;
use Membership\Model\Album;
use Membership\Model\Member as MemberModel;

class Member extends AbstractApi
{

    /**
     * Get current member
     * @return MemberModel
     */
    public function getMember()
    {
        return MemberModel::getViewer();
    }

    /**
     * Get data folder of User
     * @return string
     */
    public function getDataFolder($name=null)
    {
        $path = 'files/users/'.$this->getMember()->getId().'/';
        if($name){
            $path.=$name.'/';
        }

        FileUtils::mkdirIfNotExists($path);
        return $path;
    }

    /**
     * Get folder contains avatars
     * @return string
     */
    public function getAvatarFolder(){
        return $this->getDataFolder('avatars');
    }

    /**
     * Get folder contains photos
     * @return string
     */
    public function getPhotoFolder(){
        return $this->getDataFolder('photos');
    }

    public function getAlbumFolder(Album $album){
        return $this->getDataFolder('photos/'.$album->getId());
    }
} 