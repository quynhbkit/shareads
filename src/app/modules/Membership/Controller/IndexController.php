<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Form\Signup;
use Membership\Form\Profile as FormProfile;
use Membership\Model\Member;
use SebastianBergmann\Exporter\Exception;
use User\Model\Role;

/**
 * Index controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 */
class IndexController extends AbstractController
{

    /**
     * Module index action.
     *
     * @param string $username
     * @return void
     *
     * @Route("/members/{username:\w+}", methods={"GET"}, name="member-index")
     */
    public function indexAction($username)
    {

    }
}
