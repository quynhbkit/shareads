<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Model\Member;
use Membership\Model\MusicJackpot;
use Membership\Model\Term;
use Membership\Form\MusicJackpot as MusicJackpotForm;
use Membership\Exception\Exception;
use Membership\Model\TermRelationship;
use Membership\Query\QueryHelper;
use Phalcon\Mvc\View;

/**
 * MusicJackpot controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/music-jackpots", name="music-jackpot")
 */
class MusicJackpotController extends AbstractController
{

    protected function _getResource()
    {
        return '\Membership\Model\MusicJackpot';
    }

    /**
     * List Music Jackpot
     *
     * @return void
     *
     * @Route("/list", methods={"GET"}, name="music-jackpot-list")
     */
    public function listAction()
    {
        $options = $this->createQueryOptionsFromRequest();
        $builder = QueryHelper::prepareQuery('\Membership\Model\MusicJackpot', $options);
        $count = QueryHelper::getCount($builder);
        $items = $builder->getQuery()->execute();
        $this->view->items = $items;
        $this->view->count = $count;
    }

    /**
     * Preview Music Jackpot
     *
     * @return void
     *
     * @Route("/preview/{id:\d+}", methods={"GET"}, name="music-jackpot-preview")
     */
    public function previewAction($id)
    {
        $entity = MusicJackpot::findById($id, $this->getUserId());
        if (!$entity) {
            return $this->response->redirect(['for' => 'music-jackpot-list']);
        }
        $this->view->entity = $entity;
        if ($this->request->isAjax()) {
            $this->view->disableLevel(View::LEVEL_LAYOUT);
        }
    }

    /**
     * Add Music Jackpot
     *
     * @return void
     *
     * @Route("/add", methods={"GET","POST"}, name="music-jackpot-add")
     */
    public function addAction()
    {
        $this->assets->addCss('external/flatlab/assets/nestable/jquery.nestable.css');
        $form = new MusicJackpotForm();
        $this->view->form = $form;
        if (!$this->request->isPost()) {
            $term = new Term();
            $trees = $term->getTree($term);
            $this->view->trees = $trees;
            $this->view->treeOptions = array('values' => []);
        }
        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            return;
        }
        try {
            $user = Member::getViewer();
            $entity = $form->getEntity();
            $entity->user_id = $user->id;
            if (!$entity->save()) {
                foreach ($entity->getMessages() as $msg) {
                    $this->flashSession->error($msg);
                }
                return;
            }
            $termIds = $this->request->getPost('term_id');
            foreach ($termIds as $id) {
                $item = new TermRelationship();
                $item->setTermId($id);
                $item->setObjectId($entity->id);
                $item->save();
            }
        } catch (Exception $ex) {
            $this->flashSession->error('New object created failed!');
        }
        $this->flashSession->success('New object created successfully!');
        return $this->response->redirect(['for' => 'music-jackpot-add']);
    }

    /**
     * Edit Music Jackpot
     *
     * @return void
     *
     * @Route("/edit/{id:\d+}", methods={"GET","POST"}, name="music-jackpot-edit")
     */
    public function editAction($id)
    {
        $this->assets->addCss('external/flatlab/assets/nestable/jquery.nestable.css');
        $entity = MusicJackpot::findById($id, $this->getUserId());
        if (!$entity) {
            return $this->response->redirect(['for' => 'music-jackpot-list']);
        }
        $form = new MusicJackpotForm($entity);
        $this->view->form = $form;

        $terms = $entity->getTermRelationships();
        $oldTerms = [];
        foreach ($terms as $itemRelationship) {
            $oldTerms[] = (int)$itemRelationship->getTermId();
        }

        if (!$this->request->isPost()) {
            $term = new Term();
            $trees = $term->getTree($term);
            $this->view->trees = $trees;
            $this->view->treeOptions = array('values' => $oldTerms);
        }

        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            return;
        }
        try {
            $entity = $form->getEntity();
            $termIds = $this->request->getPost('term_id');
            foreach ($terms as $term) {
                if (!in_array($term->getTermId(), $termIds)) {
                    $term->delete();
                }
            }
            foreach ($termIds as $id) {
                if (!in_array($id, $oldTerms)) {
                    $item = new TermRelationship();
                    $item->setTermId($id);
                    $item->setObjectId($entity->id);
                    $item->save();
                }
            }
            if (!$entity->save()) {
                foreach ($entity->getMessages() as $msg) {
                    $this->flashSession->error($msg);
                }
                return;
            }
        } catch (Exception $ex) {
            $this->flashSession->error('Object updated failed!');
        }
        $this->flashSession->success('Object updated successfully!');
        return $this->response->redirect(['for' => 'music-jackpot-list']);
    }

    /**
     * Delete Music Jackpot
     *
     * @return void
     *
     * @Route("/delete", methods={"POST"}, name="music-jackpot-delete")
     */
    public function deleteAction()
    {
        $ids = $this->request->getPost('chk');
        if (!is_array($ids) || !count($ids)) {
            $this->flashSession->error('No item selected!');
        } else {
            foreach ($ids as $id) {
                $entity = MusicJackpot::findById((int)$id, $this->getUserId());
                if (!$entity) {
                    $this->flashSession->error('Object not found!');
                } else {
                    $entity->delete();
                }
            }
            $this->flashSession->success('Object deleted successfully!');
        }
        return $this->response->redirect(['for' => 'music-jackpot-list']);
    }
}
