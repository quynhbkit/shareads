<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  | Author: Piotr Gasiorowski <p.gasiorowski@vipserv.org>                  |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Core\Controller\AbstractAdminController;
use Membership\Controller\Grid\Admin\TemplatePictureGrid;
use Membership\Form\Admin\CreateTemplatePicture as CreateForm;
use Membership\Form\Admin\EditTemplatePicture as EditForm;
use Engine\Navigation;
use Membership\Model\TemplatePicture;

/**
 * @RoutePrefix("/admin/template-pictures", name="admin-template-picture")
 */
class AdminTemplatePictureController extends AbstractAdminController
{
    public function init()
    {
        $navigation = new Navigation();
        $navigation
            ->setItems(
                [
                    'index' => [
                        'href' => 'admin/template-pictures',
                        'title' => 'Template pictures',
                        'prepend' => '<i class="glyphicon glyphicon-picture"></i>'
                    ],
                    1 => [
                        'href' => 'javascript:;',
                        'title' => '|'
                    ],
                    'create' => [
                        'href' => 'admin/template-pictures/create',
                        'title' => 'Create new picture',
                        'prepend' => '<i class="glyphicon glyphicon-plus-sign"></i>'
                    ]
                ]
            );

        $this->view->navigation = $navigation;

    }

    /**
     * Main action.
     *
     * @return void
     *
     * @Get("/", name="admin-template-picture")
     */
    public function indexAction()
    {
        $grid = new TemplatePictureGrid($this->view);
        if ($response = $grid->getResponse()) {
            return $response;
        }
    }

    /**
     * Create new company.
     *
     * @return mixed
     *
     * @Route("/create", methods={"GET", "POST"}, name="admin-template-picture-create")
     */
    public function createAction()
    {
        $form = new CreateForm();
        $this->view->form = $form;

        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            return;
        }

        $entity = $form->getEntity();
        $file = $form->getFiles('picture');
        if ($file && $file->getSize() != 0) {
            $path = $entity->getFilePath($file->getName());
            $file->moveTo($path);
            $entity->filename = basename($path);
        }
        if(!$entity->save()){
            foreach($entity->getMessages() as $message){
                $this->flashSession->error($message->getMessage());
            }
            return;
        }
        $this->flashSession->success('New object created successfully!');

        return $this->response->redirect(['for' => 'admin-template-picture']);
    }

    /**
     * Edit template picture.
     *
     * @param int $id Template Picture identity.
     *
     * @return mixed
     *
     * @Route("/edit/{id:[0-9]+}", methods={"GET", "POST"}, name="admin-template-picture-edit")
     */
    public function editAction($id)
    {
        $item = TemplatePicture::findFirst($id);
        if (!$item) {
            return $this->response->redirect(['for' => 'admin-template-picture']);
        }

        $form = new EditForm($item);
        $this->view->form = $form;

        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            return;
        }
        $entity = $form->getEntity();
        $file = $form->getFiles('picture');
        if ($file && $file->getSize() != 0) {
            $path = $entity->getFilePath($file->getName());
            $file->moveTo($path);
            $entity->filename = basename($path);
        }

        if(!$entity->save()){
            foreach($entity->getMessages() as $message){
                $this->flashSession->error($message->getMessage());
            }
            return;
        }

        $this->flashSession->success('Object saved!');

        return $this->response->redirect(['for' => 'admin-template-picture']);
    }

    /**
     * View template picuture details.
     *
     * @param int $id Template Picture identity.
     *
     * @return mixed
     *
     * @Get("/view/{id:[0-9]+}", name="admin-template-picture-view")
     */
    public function viewAction($id)
    {
        $entity = TemplatePicture::findFirst($id);
        $this->view->form = $form = TextForm::factory($entity, [], []);

        $form
            ->setTitle('Template picture details')
            ->addFooterFieldSet()
            ->addButtonLink('back', 'Back', ['for' => 'admin-template-picture']);
    }

    /**
     * Delete Template picture.
     *
     * @param int $id Template picture identity.
     *
     * @return mixed
     *
     * @Get("/delete/{id:[0-9]+}", name="admin-template-picture-delete")
     */
    public function deleteAction($id)
    {
        $item = TemplatePicture::findFirst($id);
        if ($item) {
            if ($item->delete()) {
                $this->flashSession->notice('Object deleted!');
            } else {
                $this->flashSession->error($item->getMessages());
            }
        }

        return $this->response->redirect(['for' => 'admin-template-picture']);
    }
}