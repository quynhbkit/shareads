<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Model\Member;
use Membership\Model\HeavyPage;
use Membership\Form\HeavyPage as HeavyPageForm;
use Membership\Exception\Exception;

/**
 * HeavyPage controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/heavypage", name="heavypage")
 */
class HeavyPageController extends AbstractController
{

    protected function _getResource(){
        return '\Membership\Model\HeavyPage';
    }


    /**
     * Lite Page
     *
     * @return void
     *
     * @Route("/edit", methods={"GET","POST"}, name="heavypage-edit")
     */
    public function editAction()
    {
        $this->assets->addJs('external/markdown/js/bootstrap-markdown.js');

        $user = Member::getViewer();
        $heavypage = $user->heavypage;
        if(!$heavypage){
            $heavypage = new HeavyPage();
        }
        $form = new HeavyPageForm($heavypage);
        $this->view->form = $form;
        $this->view->profile = $user->profile;

        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            return;
        }
        try{
            $entity = $form->getEntity();
            $entity->user_id = $user->id;
            if(!$entity->save()){
                $this->flashSession->error('Failed to save heavypage.');
            }
        }catch(Exception $ex){
            $this->flashSession->error('New object created failed!');
        }
        $this->flashSession->success('New object created successfully!');
        return $this->response->redirect(['for' => 'heavypage-edit']);
    }
}
