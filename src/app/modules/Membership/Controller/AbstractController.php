<?php
namespace Membership\Controller;

use Core\Controller\AbstractController as CoreAbstractController;
use Membership\Query\QueryOptions;
use User\Model\User;
use Phalcon\Mvc\Dispatcher;

/**
 * MemberAbstract controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 */
class AbstractController extends CoreAbstractController
{
    protected function _getResource()
    {
        return '\Membership\Model\Member';
    }

    public function beforeExecuteRoute()
    {
        $isAllowed = $this->_isAllowed();
        if (!$isAllowed) {
            $this->response->redirect(['for' => 'login']);
            $this->view->disable();
            return false;
        }
    }

    /**
     * Get ID of curren user
     * @return int
     */
    public function getUserId()
    {
        return User::getViewer()->getId();
    }

    protected function _isAllowed()
    {
        $dispatcher = $this->dispatcher;
        $action = $dispatcher->getActionName();
        $viewer = User::getViewer();
        $acl = $this->getDI()->get('core')->acl();
        return $acl->isAllowed($viewer->getRole()->name, $this->_getResource(), $action);
    }

    protected function _setupAssets()
    {
        parent::_setupAssets();
        $this->assets
            ->addJs('external/autocomplete/dist/jquery.autocomplete.min.js')
            ->addJs('external/bootstrap/js/bootstrap.min.js')
            ->addJs('//ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.min.js', false)
            ->addJs('//ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular-resource.min.js', false)
            ->addJs('external/ui-bootstrap-tpls-0.11.0.min.js')
            ->addJs('assets/js/membership/main.js')
            ->addJs('assets/js/membership/util.js');
    }

    public function error404()
    {
        $dispatcher = $this->getDI()->get('dispatcher');
        $this->getDI()->getEventsManager()->fire(
            'dispatch:beforeException',
            $dispatcher,
            new Dispatcher\Exception()
        );
    }

    public function sendJsonContent($data)
    {
        $this->view->disable();
        $res = $this->response;

        $res->setJsonContent($data);
        $res->setContentType('application/json');
        $res->send();
    }

    /**
     * Createas a QueryOptions object from the request
     * @return QueryOptions
     */
    protected function createQueryOptionsFromRequest()
    {

        $options = QueryOptions::create();
        // Limit the query
        if ($this->request->hasQuery('limit')) {
            $options->setLimit((int)$this->request->getQuery('limit'));
        }
        // Offset the query
        if ($this->request->hasQuery('offset')) {
            $options->offset = (int)$this->request->getQuery('offset');
        }
        // Sort the query
        if ($this->request->hasQuery('sortBy')) {
            $options->sortBy = $this->request->getQuery('sortBy');
            $options->sortOrder = $this->request->getQuery('sortOrder');
        }
        return $options;
    }
}
