<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Model\TemplatePicture;
use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;
use Membership\Model\UserTemplate;
/**
 * Class TemplateController
 * @package Membership\Controller
 *
 * @RoutePrefix('/templates',name='templates');
 */
class TemplateController extends AbstractController
{
    const PAGE_SIZE = 16;
    protected function _getResource()
    {
        return '\Membership\Model\TemplatePicture';
    }


    /**
     * Module classic action.
     *
     * @param string $type
     * @return void
     *
     * @Route("/classic/{type:[a-zA-Z\-]+}", methods={"GET"}, name="templates-classic")
     */
    public function classicAction($type)
    {
        $page = $this->request->get('page','int',1);
        $builder = TemplatePicture::getBuilder('t')
            ->columns('t.*')
            ->innerJoin('Membership\Model\Term','t.term_id = te.id', 'te')
            ->where('te.slug = ?0', [$type]);

        $paginator = new Paginator(array(
            "builder" => $builder,
            "limit"=> self::PAGE_SIZE,
            "page" => $page
        ));

        $this->view->paginator = $paginator->getPaginate();
    }

    /**
     * Select picture for classic theme
     *
     * @Route("/classic/update", methods={"POST"}, name="templates-classic-update")
     */
    public function updateAction(){
        if(!$this->request->isAjax() || !$this->request->isPost()){
            return;
        }

        $result = [
            'status'=>false
        ];
        $param = $this->request->getPost('param');
        if(!$param){
            $this->sendJsonContent($result);
            return;
        }
        $userId = $this->getUserId();
        $entity = UserTemplate::getTemplateParam($userId, $param);
        $_POST['user_id'] = $userId;
        if(!$entity){
            $entity = new UserTemplate();
        }
        if(!$entity->save($_POST)){
            $this->sendJsonContent($result);
            return;
        }
        $result['status']=true;
        $this->sendJsonContent($result);
    }
}
