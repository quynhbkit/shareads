<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Model\Data;

/**
 * Data controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/data", name="data")
 */
class DataController extends AbstractController
{
    /**
     * @var Data
     */
    protected $_provider =null;
    protected function _getResource(){
        return '\Membership\Model\Data';
    }

    protected function _getProvider(){
        if(!$this->_provider){
            $this->_provider = new Data();
        }
        return $this->_provider;
    }

    /**
     * Get country
     * @return void
     *
     * @Route("/countries", methods={"GET"}, name="data-country")
     */
    public function countryAction(){
        $query = $this->request->get('query');
        $rows =Data::getCountries($query);
        $this->sendJsonContent(['records'=>$rows]);
    }

    /**
     * Get city
     * @return void
     *
     * @Route("/cities", methods={"GET"}, name="data-country")
     */
    public function cityAction(){
        $query = $this->request->get('query');
        $rows = Data::getCities($query);
        $this->sendJsonContent(['records'=>$rows]);
    }

    /**
     * Get state
     * @return void
     *
     * @Route("/states", methods={"GET"}, name="data-country")
     */
    public function stateAction(){
        $query = $this->request->get('query');
        $rows = Data::getStates( $query);
        $this->sendJsonContent(['records'=>$rows]);
    }
}
