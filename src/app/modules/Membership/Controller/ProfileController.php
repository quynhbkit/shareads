<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Form\Profile as FormProfile;
use Membership\Model\Member;
use Membership\Model\Country;
use Membership\Model\City;
use Membership\Model\State;
use Membership\Exception\Exception;

/**
 * Profile controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/profile", name="profile")
 */
class ProfileController extends AbstractController
{

    protected function _getResource(){
        return '\Membership\Model\Profile';
    }

    /**
     * Profile detail
     * @return void
     *
     * @Route("/", methods={"GET"}, name="profile-index")
     */
    public function indexAction()
    {
        $this->assets->addJs('external/FileAPI/dist/FileAPI.min.js');
        $this->assets->addJs('assets/js/membership/profile/avatar.js');
        $this->assets->addJs('assets/js/membership/profile/module.js');
        $this->assets->addJs('assets/js/membership/imagecrop.js');
        $this->assets->addJs('external/jcrop/js/jquery.Jcrop.min.js');
        $this->assets->addCss('external/jcrop/css/jquery.Jcrop.min.css');
        $this->assets->addJs('external/angular-file-upload.min.js');

        $user = Member::getViewer();
        $profile=$user->profile;
        $profile->avatar_url = $profile->getAvatarUrl();
        $this->view->profile = $profile;
    }

    /**
     * Edit personal information
     * @return void
     *
     * @Route("/edit", methods={"GET", "POST"}, name="profile-edit")
     */
    public function editAction()
    {
        // $this->city();
        $this->assets->addJs('external/FileAPI/dist/FileAPI.min.js');
        $this->assets->addJs('assets/js/membership/profile/avatar.js');
        $this->assets->addJs('assets/js/membership/profile/module.js');
        $this->assets->addJs('assets/js/membership/profile/edit.js');
        $this->assets->addJs('assets/js/membership/imagecrop.js');
        $this->assets->addJs('external/jcrop/js/jquery.Jcrop.min.js');
        $this->assets->addCss('external/jcrop/css/jquery.Jcrop.min.css');
        $this->assets->addJs('external/angular-file-upload.min.js');
        $this->assets->addJs('external/markdown/js/bootstrap-markdown.js');

        $user = Member::getViewer();
        $profile=$user->profile;
        $form = new FormProfile($profile);
        $this->view->form = $form;
        $profile->avatar_url = $profile->getAvatarUrl();
        $this->view->profile = $profile;
        $isPost = $this->request->isPost();
        if(!$isPost){
            if(($country = $profile->country)){
                $form->setValue('countrytext', $country->name);
            }
            if(($city = $profile->city)){
                $form->setValue('citytext', $city->name);
            }
            if(($state = $profile->state)){
                $form->setValue('statetext', $state->name);
            }
        }
        if (!$isPost || !$form->isValid(null, true)) {
            return;
        }
        $manager = $this->getDI()->getTransactions();
        $transaction= $manager->get();
        try{
            $profile = $form->getEntity();
            if(!$profile->save()){
                $transaction->rollback('Failed to save profile.');
            } else{
                $transaction->commit();
            }
        }catch(Exception $ex){
            $this->flashSession->error('New object created failed!');
        }
        $this->flashSession->success('New object created successfully!');
        return $this->response->redirect(['for' => 'profile-index']);
    }

    /**
     * Get avatar of user
     * @return void
     *
     * @Route("/avatar", methods={"GET","PUT","DELETE"}, name="profile-avatar")
     */
    public function avatarAction(){
        $identity = $this->getDI()->get('core')->auth()->getIdentity();
        $user = Member::findFirst($identity);
        $profile=$user->profile;
        if($this->request->isPut()){
            $this->_updateAvatar($profile);
        }else if($this->request->isDelete()){
            $profile->deleteAvatar();
        }
        $this->sendJsonContent(['avatar'=>$profile->getAvatarUrl()]);
    }

    protected function _updateAvatar($profile){
        $json =$this->request->getJsonRawBody();
        if($json){
            $filename = @$json->filename;
            $avatar = @$json->avatar;
            $profile->updateAvatar($filename, $avatar);
        }
        return $profile;
    }

    protected function _scanFiles($dir){
        $txtFiles = [];
        // Get all file names.
        $files = scandir($dir);
        // Iterate files.
        foreach ($files as $file) {
            if ($file == "." || $file == "..") {
                continue;
            }
            $txtFiles[]=$file;
        }

        return $txtFiles;
    }
    public function city(){
        set_time_limit(0);
        $dir = '/var/www/html/shareads/_db/cities/USA/';
        $files = $this->_scanFiles($dir);
        for($i=0; $i<count($files); $i++){
            $file = $dir .$files[$i];
            $fp = fopen($file,'r');
            while(($data =fgetcsv($fp))){
                if(count($data)<2){
                    continue;
                }
                $city = $data[0];
                if(count($data)>=3){
                    $state = trim($data[1]);
                    $country = trim($data[2]);
                }else{
                    $state = $city;
                    $country = trim($data[1]);
                }
                if($country=='USAe'){
                    $country='USA';
                }
                $this->_importCity($city, $state, $country);
            }
            fclose($fp);
        }

    }

    protected function _importCity($cityName, $stateName, $countryName){
        $country = Country::findFirst(['conditions'=>'name=?1', 'bind'=>[1=>$countryName]]);
        if(!$country){
            $country = new Country();
            $country->name = $countryName;
        }

        if(!($state = State::findFirst(['conditions'=>'name=?1', 'bind'=>[1=>$stateName]]))){
            $state = new State();
            $state->name = $stateName;
        }
        $country->save();
        $state->country_id=$country->id;
        $state->save();

        $city = new City();
        $city->name = $cityName;
        $city->country_id = $country->id;
        $city->state_id = $state->id;

        return $city->save();
    }

}
