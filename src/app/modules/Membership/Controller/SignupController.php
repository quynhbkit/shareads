<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Form\Signup;
use Membership\Form\Profile as FormProfile;
use SebastianBergmann\Exporter\Exception;
use User\Model\Role;

/**
 * Signup controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 */
class SignupController extends AbstractController
{
    /**
     * Module index action.
     *
     * @return void
     *
     * @Route("/register", methods={"POST", "GET"}, name="member-register")
     */
    public function registerAction()
    {
        $this->assets->addCss('external/bootstrap/css/bootstrap.min.css');
        $this->assets->addCss('external/flatlab/css/bootstrap-reset.css');
        $this->assets->addCss('external/flatlab/assets/font-awesome/css/font-awesome.css');
        $this->assets->addCss('external/flatlab/css/style.css');
        $this->assets->addCss('external/flatlab/css/style-responsive.css');

        $form = new Signup();
        $this->view->form = $form;

        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            return;
        }
        $manager = $this->getDI()->getTransactions();
        $transaction = $manager->get();
        try {
            $user = $form->getEntity('member');
            $profile = $form->getEntity('profile');

            $password = $form->getValue('password');
            $repeatPassword = $form->getValue('retypepassword');
            if ($password != $repeatPassword) {
                $form->addError("Passwords doesn't match!", 'password');
                return;
            }

            $user->setPassword($user->password);
            $user->role_id = Role::getDefaultRole()->id;
            if (!$user->save()) {
                $transaction->rollback('Failed to save profile.');
            } else {
                $profile->user_id = $user->id;
                if (!$profile->save()) {
                    $transaction->rollback('Failed to save profile.');
                } else {
                    $transaction->commit();
                    $this->core->auth()->authenticate($user->id);
                    return $this->response->redirect(['for' => 'profile-index']);
                }
            }
        } catch (Exception $ex) {
            $this->flashSession->error('New object created failed!');
        }
        $this->flashSession->success('New object created successfully!');

        return $this->response->redirect(['for' => 'member-register']);
    }
}
