<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller;

use Membership\Model\Album;
use Membership\Model\Member;
use Membership\Form\PictureCaption as PictureCaptionForm;
use Membership\Exception\Exception;
use Membership\Model\Photo;
use Sabre\VObject\Property\DateTime;
use Engine\Widget\Element as WidgetElement;

/**
 * PictureCaption controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/picture-caption", name="picture-caption")
 */
class PictureCaptionController extends AbstractController
{

    protected function _getResource(){
        return '\Membership\Model\Photo';
    }

    /**
     * Lite Page
     *
     * @return void
     *
     * @Route("/edit", methods={"GET","POST"}, name="picture-caption-edit")
     */
    public function editAction()
    {
        $form = new PictureCaptionForm();
        $widget = new WidgetElement(5,[], $this->getDI());
        if($widget){
            $this->view->widget = $widget->render();
        }
        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            $this->view->form = $form;
            return;
        }
        try{
            $user = Member::getViewer();
            $entity = $form->getEntity();
            $entity->user_id = $user->id;
            $album = Album::getPictureCaptionAlbum($user->getId());
            $path = $this->membership->member()->getAlbumFolder($album);
            $file = $form->getFiles('photo');
            if ($file && $file->getSize() != 0) {
                $path = $path.$file->getName();
                $file->moveTo($path);
                $entity->filename = $file->getName();
            }
            $entity->setAlbum($album);
            if(!$entity->save()){
                $this->flashSession->error('Failed to save data.');
            }
        }catch(Exception $ex){
            $this->flashSession->error('New object created failed!');
        }
        $this->flashSession->success('New object created successfully!');
        return $this->response->redirect(['for' => 'picture-caption-edit']);
    }

    /**
     * Delete a Photo
     * @Route("/photos/{id:\d+}", methods={"POST"}, name="picture-caption-delete")
     */
    public function deleteAction($id){
        $album = Album::getPictureCaptionAlbum($this->getUserId());
        if(!$album){
            return $this->sendJsonContent(['status'=>false,'id'=>$id]);
        }
        $photo = Photo::findFirst(['id = ?0 AND album_id = ?1', 'bind'=>[(int)$id, $album->getId()]]);

        if(!$photo || !$photo->delete()){
            return $this->sendJsonContent(['status'=>false,'id'=>$id]);
        }
        $this->sendJsonContent(['status'=>true,'id'=>$id]);
    }
}
