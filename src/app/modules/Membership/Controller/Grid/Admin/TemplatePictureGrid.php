<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Membership\Controller\Grid\Admin;

use Core\Controller\Grid\CoreGrid;
use Engine\Form;
use Engine\Grid\GridItem;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\View;
use Membership\Model\Term;

/**
 * Layout Image grid.
 *
 * @category  PhalconEye
 * @package   Core\Controller\Grid\Admin
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class TemplatePictureGrid extends CoreGrid
{
    /**
     * Get main select builder.
     *
     * @return Builder
     */
    public function getSource()
    {
        $builder = new Builder();
        $builder
            ->columns(['t.*', 'te.name'])
            ->addFrom('Membership\Model\TemplatePicture', 't')
            ->leftJoin('Membership\Model\Term', 't.term_id = te.id', 'te')
            ->orderBy('t.id DESC');

        return $builder;
    }

    /**
     * Get item action (Edit, Delete, etc).
     *
     * @param GridItem $item One item object.
     *
     * @return array
     */
    public function getItemActions(GridItem $item)
    {
        return [
            'Edit' => ['href' => ['for' => 'admin-template-picture-edit', 'id' => $item['t.id']]],
            'Delete' => [
                'href' => ['for' => 'admin-template-picture-delete', 'id' => $item['t.id']],
                'attr' => ['class' => 'grid-action-delete']
            ]
        ];
    }

    /**
     * Initialize grid columns.
     *
     * @return array
     */
    protected function _initColumns()
    {
        $this
            ->addTextColumn(
                't.id',
                'ID',
                [
                    self::COLUMN_PARAM_TYPE => Column::BIND_PARAM_INT,
                    self::COLUMN_PARAM_OUTPUT_LOGIC =>
                        function (GridItem $item, $di) {
                            $url = $di->get('url')->get(
                                ['for' => 'admin-template-picture-view', 'id' => $item['t.id']]
                            );
                            $url =$item->getObject()->t->getUrl();
                            return sprintf('<a href="%s" target="_blank">%s</a>', $url, $item['t.id']);
                        }
                ]
            )
            ->addTextColumn('t.title', 'Title')
            ->addTextColumn('t.filename', 'File name')
            ->addTextColumn('t.description', 'Description')
            ->addSelectColumn(
                'te.name',
                'Type',
                ['hasEmptyValue' => true, 'using' => ['name', 'name'], 'elementOptions' => Term::find(['taxonomy=?0','bind'=>['template-picture']])],
                [
                    self::COLUMN_PARAM_USE_HAVING => false,
                    self::COLUMN_PARAM_USE_LIKE => false,
                    self::COLUMN_PARAM_OUTPUT_LOGIC =>
                        function (GridItem $item) {
                            return $item['name'];
                        }
                ]
            )
            ->addTextColumn('t.creation_date', 'Creation Date');
    }
}