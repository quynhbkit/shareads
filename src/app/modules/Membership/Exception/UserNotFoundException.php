<?php
/**
 * Created by PhpStorm.
 * User: Quynh
 * Date: 7/12/14
 * Time: 10:41 AM
 */

namespace Membership\Exception;

class UserNotFoundException extends Exception{
    protected $message = 'The user you request could not be found';
} 