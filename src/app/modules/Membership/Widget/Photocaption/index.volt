{% extends "Core/View/layouts/widget.volt" %}

{% block content %}
    <?php $this->assets->addCss('external/flatlab/css/gallery.css')?>
    <?php $this->assets->addJs('external/flatlab/assets/fancybox/source/jquery.fancybox.js')?>
    <?php $this->assets->addJs('assets/js/membership/picture-caption.js')?>
    <ul class="grid cs-style-4">
        {% for photo in photos %}
            <li>
                <figure>
                    <img src="{{ photo.getUrl() }}" alt="">
                    <figcaption>
                        <span>{{ photo.description }}</span>
                        <a class="fancybox" rel="group" href="{{ photo.getUrl() }}">Take a look</a>
                    </figcaption>
                    <a href="javascript:" class="trash" title="{{ 'delete' | i18n }}" onclick="sha.album.Photo.delete({{ photo.getId() }}, this)"><i class="icon-trash"></i></a>
                </figure>
            </li>
        {% endfor %}
    </ul>
{% endblock %}