<?php
namespace Membership\Form;

use Core\Form\FileForm;
use Engine\Db\AbstractModel;
use Phalcon\Validation\Validator\PresenceOf;
use Membership\Model\Photo as PhotoEntity;

/**
 * Upload picture caption
 *
 * @category  Membership
 * @package   Membership\Form
 * @author    Quynh Le <quynhbkit@gmail.com>
 * @copyright 2014 ShareAds.us
 * @license   ShareAds.us
 * @link      http://www.shareads.us/
 */
class PictureCaption extends FileForm
{
    /**
     * Create form.
     *
     * @param AbstractModel $entity Entity object.
     */
    public function __construct(AbstractModel $entity = null)
    {
        parent::__construct();

        if (!$entity) {
            $entity = new PhotoEntity();
        }
        $this->addEntity($entity);
    }

    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this->setAsMultipart()
            ->setTitle('Picture Caption');

        $this->addFile('photo','Photo',null, true,null,[],[]);
        $this->addTextArea('description', 'Description', null, null, [], ['rows'=>5]);

        $this->addFooterFieldSet()
            ->addButton('Submit');

        $this->setAttribute('role', 'form');
        $this->_setValidation();
    }

    protected function _setValidation()
    {
        $this->getValidation()
            ->add('description', new PresenceOf());
    }

    public function getLayoutView()
    {
        return 'Membership/View/partials/form/basic';
    }


    protected function _resolveView($view, $module = 'Core')
    {
        $dispatcher = $this->getDI()->get('dispatcher');
        return parent::_resolveView($view, $dispatcher->getModuleName());
    }
}