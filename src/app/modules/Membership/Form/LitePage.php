<?php
namespace Membership\Form;

use Core\Form\CoreForm;
use Engine\Db\AbstractModel;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Membership\Model\LitePage as LitePageEntity;

/**
 * Signup new user.
 *
 * @category  Membership
 * @package   Membership\Form
 * @author    Quynh Le <quynhbkit@gmail.com>
 * @copyright 2014 ShareAds.us
 * @license   ShareAds.us
 * @link      http://www.shareads.us/
 */
class LitePage extends CoreForm
{
    /**
     * Create form.
     *
     * @param AbstractModel $entity Entity object.
     */
    public function __construct(AbstractModel $entity = null)
    {
        parent::__construct();

        if (!$entity) {
            $entity = new LitePageEntity();
        }
        $this->addEntity($entity);
    }

    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this
            ->setTitle('Lite Page')
            ->setDescription('Edit lite page')
            ->setAttribute('autocomplete', 'off');

        $this->addTextArea('content', 'Content', null, null, [], ['rows'=>25, 'data-provide'=>"markdown"]);

        $this->addFooterFieldSet()
            ->addButton('Submit');

        $this->setAttribute('role', 'form');
    }

    public function getLayoutView()
    {
        return 'Membership/View/partials/form/basic';
    }


    protected function _resolveView($view, $module = 'Core')
    {
        $dispatcher = $this->getDI()->get('dispatcher');
        return parent::_resolveView($view, $dispatcher->getModuleName());
    }
}