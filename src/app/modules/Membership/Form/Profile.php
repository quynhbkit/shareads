<?php
namespace Membership\Form;

use Core\Form\CoreForm;
use Engine\Db\AbstractModel;
use Engine\Form\FieldSet;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use User\Model\Role;
use Membership\Model\Member;
use Membership\Model\Profile as MemberProfile;
use Membership\Form\Element\Gender;
use Membership\Form\Element\Country;

/**
 * Signup new user.
 *
 * @category  Membership
 * @package   Membership\Form
 * @author    Quynh Le <quynhbkit@gmail.com>
 * @copyright 2014 ShareAds.us
 * @license   ShareAds.us
 * @link      http://www.shareads.us/
 */
class Profile extends CoreForm
{
    /**
     * Create form.
     *
     * @param AbstractModel $entity Entity object.
     */
    public function __construct(AbstractModel $entity = null)
    {
        parent::__construct();

        if (!$entity) {
            $profile = new MemberProfile();
            $this->addEntity($profile);
        }

        $this->addEntity($entity);
    }

    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this
            ->setTitle('Edit profile')
            ->setDescription('Edit personal information');

        $this->addText('firstname', 'First name', null, null, [], [])
            ->addText('lastname', 'Last name', null, null, [], [])
            ->addText('display_name', 'Display name', null, null, [], [])
            ->addSelect('gender','Gender',null,[0=>'Male',1=>'Female'])
            ->addText('yearofbirth', 'Year of birth', null, null, [], [])
            ->addText('mobile', 'Mobile', null, null, [], [])
            ->addText('phone', 'Phone', null, null, [], [])
            ->addText('occupation', 'Occupation', null, null, [], [])
            ->addText('citytext', 'City', null, null, [], [])
            ->addText('statetext', 'State/Province', null, null, [], [])
            ->addText('countrytext', 'Country', null, null, [], [])
            ->addText('zipcode', 'Zipcode', null, null, [], [])
            ->addText('street', 'Address', null, null, [], [])
            ->addText('apartment', 'Apartment #', null, null, [], [])
            ->addTextArea('about', 'About me', null, null, [], ['rows'=>5, 'data-provide'=>"markdown"])
            ->addHidden('country_id', null, [], [])
            ->addHidden('state_id', null, [], [])
            ->addHidden('city_id', null, [], []);

        $this->addFooterFieldSet()
            ->addButton('Save');

        $this->_setValidation();
        $this->setAttribute('role', 'form');
        $this->setAttribute('class', 'form-horizontal');
    }

    /**
     * Set form validation.
     *
     * @param FieldSet $content Content fieldset.
     *
     * @return void
     */
    protected function _setValidation()
    {
        $this->getValidation()
            ->add('firstname', new PresenceOf())
            ->add('lastname', new PresenceOf())
            ->add('gender', new PresenceOf());
    }

    public function getLayoutView()
    {
        return 'Membership/View/partials/form/horizontal';
    }

    protected function _resolveView($view, $module = 'Core')
    {
        $dispatcher = $this->getDI()->get('dispatcher');
        return parent::_resolveView($view, $dispatcher->getModuleName());
    }
}