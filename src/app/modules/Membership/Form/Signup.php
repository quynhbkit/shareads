<?php
namespace Membership\Form;

use Core\Form\CoreForm;
use Engine\Db\AbstractModel;
use Engine\Form\FieldSet;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use User\Model\Role;
use Membership\Model\Member;
use Membership\Model\Profile;
use Membership\Form\Element\Gender;

/**
 * Signup new user.
 *
 * @category  Membership
 * @package   Membership\Form
 * @author    Quynh Le <quynhbkit@gmail.com>
 * @copyright 2014 ShareAds.us
 * @license   ShareAds.us
 * @link      http://www.shareads.us/
 */
class Signup extends CoreForm
{
    /**
     * Create form.
     *
     * @param AbstractModel $entity Entity object.
     */
    public function __construct(AbstractModel $entity = null)
    {
        parent::__construct();

        if (!$entity) {
            $entity = new Member();
            $profile = new Profile();

            $this->addEntity($profile, 'profile');
        } else {
            $profile = $entity->getProfile();
            $this->addEntity($profile, 'profile');
        }

        $this->addEntity($entity, 'member');
    }

    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this
            ->setTitle('Registration now')
            ->setDescription('Sign up new user.')
            ->setAttribute('autocomplete', 'off');

        $info = new FieldSet('info');
        $info->setLegend('Enter your personal details below');
        $this->addFieldSet($info);

        $info->addText('firstname', 'First name', null, null, [], ['autocomplete' => 'off', 'placeholder' => 'First Name'])
            ->addText('lastname', 'Last name', null, null, [], ['autocomplete' => 'off', 'placeholder' => 'Last Name'])
            ->addText('email', null, null, null, [], ['placeholder' => 'Email'])
            ->add(new Gender('gender', [], ['autocomplete' => 'off']));

        $acc = new FieldSet('info');
        $acc->setLegend('Enter your account details below');
        $this->addFieldSet($acc);
            $acc->addText('username', null, null, null, [], ['autocomplete' => 'off', 'placeholder' => 'Username'])
            ->addPassword('password', null, null, [], ['autocomplete' => 'off', 'placeholder' => 'Password'])
            ->addPassword('retypepassword', null, null, [], ['autocomplete' => 'off', 'placeholder' => 'Re-type password']);


        $this->addFooterFieldSet()
            ->addButton('signup', 'Sign up', true, null, [], ['class' => 'btn btn-lg btn-login btn-block']);

        $this->_setValidationInfo($info);
        $this->_setValidationAcc($acc);
    }

    /**
     * Set form validation.
     *
     * @param FieldSet $content Content fieldset.
     *
     * @return void
     */
    protected function _setValidationInfo($content)
    {
        $content->getValidation()
            ->add('email', new Email())
            ->add('firstname', new PresenceOf())
            ->add('lastname', new PresenceOf())
            ->add('gender', new PresenceOf());
    }

    /**
     * Set form validation.
     *
     * @param FieldSet $content Content fieldset.
     *
     * @return void
     */
    protected function _setValidationAcc($content)
    {
        $content->getValidation()
            ->add('username', new PresenceOf())
            ->add('password', new PresenceOf())
            ->add('retypepassword', new PresenceOf());
    }


    public function getLayoutView()
    {
        return 'Membership/View/partials/form/signup';
    }

    protected function _resolveView($view, $module = 'Core')
    {
        $dispatcher = $this->getDI()->get('dispatcher');
        return parent::_resolveView($view, $dispatcher->getModuleName());
    }
}