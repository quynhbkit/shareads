<?php
/**
 * Created by PhpStorm.
 * User: quynh.le
 * Date: 5/15/14
 * Time: 4:59 PM
 */

namespace Membership\Form\Element;

use Engine\Form\Element\Radio;
class Gender extends Radio{

    public function __construct($name, array $options = [], array $attributes = [])
    {
        parent::__construct($name, $options, $attributes);

        $this->setOption('elementOptions', array(
            0=>'Male',
            1=>'Female'
        ));
        $template = '<label class="label_radio col-lg-6 col-sm-6" for="radio-01">
            <input type="radio" value="%s"' . $this->_renderAttributes() . '%s%s/> %s
        </label>';

        $this->setOption('htmlTemplate',$template);
    }
}