<?php
/**
 * Created by PhpStorm.
 * User: quynh.le
 * Date: 5/15/14
 * Time: 4:59 PM
 */

namespace Membership\Form\Element;

use Engine\Form\Element\Select;
use Membership\Model\City as CityModel;
class City extends Select{
    const CACHE_KEY = 'city_data';
    public function __construct($name, array $options = [], array $attributes = [])
    {
        parent::__construct($name, $options, $attributes);
        $items=[];
        foreach($this->_getOptionsData() as $row){
            $items[$row->id]=$row->name;
        }
        $this->setOption('elementOptions', $items);
    }

    protected function _getOptionsData(){
        $cache = $this->getDI()->get('cacheData');
        if(!($rows = $cache->get(self::CACHE_KEY))){
            $rows = CityModel::find ();
            $cache->save(self::CACHE_KEY, $rows);
        }

        return $rows;
    }
}