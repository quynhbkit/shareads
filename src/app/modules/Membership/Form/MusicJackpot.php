<?php
namespace Membership\Form;

use Core\Form\CoreForm;
use Engine\Db\AbstractModel;
use Engine\Form\FieldSet;
use Phalcon\Validation\Validator\PresenceOf;
use Membership\Model\MusicJackpot as MusicJackpotModel;


/**
 * Create MusicJackpot.
 *
 * @category  Membership
 * @package   Membership\Form
 * @author    Quynh Le <quynhbkit@gmail.com>
 * @copyright 2014 ShareAds.us
 * @license   ShareAds.us
 * @link      http://www.shareads.us/
 */
class MusicJackpot extends CoreForm
{
    protected $_openned = false;
    protected $_closed = false;
    /**
     * Create form.
     *
     * @param AbstractModel $entity Entity object.
     */
    public function __construct(AbstractModel $entity = null)
    {
        parent::__construct();

        if (!$entity) {
            $profile = new MusicJackpotModel();
            $this->addEntity($profile);
        }

        $this->addEntity($entity);
    }

    public function openTag(){
        if(!$this->_openned){
            return parent::openTag();
            $this->_openned = true;
        }
    }

    public function closeTag(){
        if(!$this->_closed){
            $this->_closed = true;
        }else{
            return parent::closeTag();
        }
    }

    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this
            ->setTitle('Create MusicJackpot');
        //$col1 =  new FieldSet('col1', null, ['class'=>'col-lg-6']);
        //$this->addFieldSet($col1);
        $this->addText('title', 'Contest', null, null, [], [])
            ->addText('name', 'Entry name', null, null, [], [])
            ->addText('entry_date', 'Entry date', null, null, [], [])
            ->addText('cover', 'Cover?', null, null, [], [])
            ->addText('url', 'URL link', null, null, [], [])
            ->addTextArea('embed_code', 'EMBED code', null, null, ['escape'=>false], [])
            ->addText('owner', 'Who Owns This Song', null, null, [], [])
            ->addText('connection', 'Your connection to the Owner', null, null, [], [])
            ->addText('involvement', 'Your involvement with this Video', null, null, [], [])
            ->addTextArea('about', 'About our Video, Song, Singer, Group or Band, etc.', null, null, [], ['rows' => 5, 'data-provide' => "markdown"]);

        //$col2 = new FieldSet('col2', null, ['class'=>'col-lg-6']);
        //$this->addFieldSet($col2);
        $this->addFooterFieldSet()
            ->addButton('Save')
            ->addButtonLink('Cancel',null,['for'=>'music-jackpot-list'],[], ['class'=>'btn btn-default']);

        $this->_setValidation($this);
        $this->setAttribute('role', 'form');
    }

    /**
     * Set form validation.
     *
     * @param FieldSet $content Content fieldset.
     *
     * @return void
     */
    protected function _setValidation($content)
    {
        $content->getValidation()
            ->add('title', new PresenceOf())
            ->add('name', new PresenceOf())
            ->add('url', new PresenceOf())
            ->add('embed_code', new PresenceOf());
    }

    public function getLayoutView()
    {
        return 'Membership/View/partials/form/basic';
    }

    protected function _resolveView($view, $module = 'Core')
    {
        $dispatcher = $this->getDI()->get('dispatcher');
        return parent::_resolveView($view, $dispatcher->getModuleName());
    }
}