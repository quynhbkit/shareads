<?php
namespace Membership\Form\Admin;

use Core\Form\FileForm;
use Engine\Db\AbstractModel;
use Engine\Form\FieldSet;
use Phalcon\Validation\Validator\PresenceOf;
use Membership\Model\Term;
use Membership\Model\TemplatePicture;

class CreateTemplatePicture extends FileForm
{
    /**
     * Create form.
     *
     * @param AbstractModel $entity Entity object.
     */
    public function __construct(AbstractModel $entity = null)
    {
        parent::__construct();

        if (!$entity) {
            $entity = new TemplatePicture();
        }

        $this->addEntity($entity);
    }

    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this->setAsMultipart()
            ->setTitle('Template pictures Creation')
            ->setDescription('Create new template picture.');

        $content = $this->addContentFieldSet()
            ->addText('title', null, null, null, [], [])
            ->addTextArea('description', null, null, null, [], ['rows'=>5])
            ->addFile('picture','Picture',null, true,null,[],[])
            ->addSelect('term_id', 'Type', 'Select type', Term::find(['taxonomy=?0','bind'=>['template-picture']]), null, ['using' => ['id', 'name']]);

        $this->addFooterFieldSet()
            ->addButton('create')
            ->addButtonLink('cancel', 'Cancel', ['for' => 'admin-template-picture']);

        $this->_setValidation($content);
    }

    /**
     * Set form validation.
     *
     * @param FieldSet $content Content fieldset.
     *
     * @return void
     */
    protected function _setValidation($content)
    {
        $content->getValidation()
            ->add('title', new PresenceOf())
            ->add('term_id', new PresenceOf());
    }
}