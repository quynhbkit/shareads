<?php

namespace Membership\Query;

use Phalcon\DI;
use Phalcon\Mvc\Model\Query\BuilderInterface;

class QueryHelper
{

    public static function prepareQuery($modelClassName, QueryOptions $options)
    {
        $modelsManager = DI::getDefault()->get('modelsManager');
        $query = $modelsManager->createBuilder()->from(array('t' => $modelClassName));

        // Prepare for where
        $query->where('1 = 1');

        if ($options->ids !== null) {
            // Get where
            $query->inWhere('t.id', $options->ids);
        }

        // Limit the query
        if ($options->limit !== null) {
            $query->limit($options->limit);
        }

        // Offset the query
        if ($options->offset !== null) {
            $query->offset($options->offset);
        }

        // Sort the query
        if ($options->sortBy !== null) {
            $str = 't.' . $options->sortBy;
            if ($options->sortOrder !== null) {
                $str .= ' ' . $options->sortOrder;
            }
            $query->orderBy($str);
        }

        return $query;
    }

    public static function getCount(BuilderInterface $builder)
    {
        $countBuilder = clone $builder;
        $countBuilder->columns('count(*) as total')
            ->limit(null);
        return $countBuilder->getQuery()->execute()->getFirst()->total;
    }
}
