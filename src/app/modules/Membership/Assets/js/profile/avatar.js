(function ($, root, angular, undefined) {
    var ns = 'sha.membership.profile.avatar';
    var module = root.ns(ns);
    var profile = window.profile;

    module.Controller = function ($scope, MemberService, ImageCropService) {
        var self = this;
        $scope.hasAvatar = false;
        function setHasAvatar(value) {
            $scope.hasAvatar = value;
        };

        function init() {
            if (profile.avatar_url) {
                var avatarUrl = profile.avatar_url;
                setHasAvatar(true);
                $scope.avatarUrl = avatarUrl;
            } else {
                setHasAvatar(false);
            }
        }
        $scope.deleteAvatar = function () {
            MemberService.deleteAvatar()
                .then(function (result) {
                    setHasAvatar(false);
                });
        };
        $scope.showAvatar = function (files) {
            var file = files[0];
            var dlg = ImageCropService.crop(file);
            dlg.then(function (img) {
                var filename = file.name;
                MemberService.uploadAvatar(img, filename)
                    .then(function (result) {
                        setHasAvatar(true);
                        $scope.avatarUrl = result.avatar;
                    });
            });
        };

        $scope.onFileSelect = function (files) {
            $scope.showAvatar(files);
        }
        init();
    };
    return module;
})(jQuery, PhalconEye, angular);
