(function ($, root, angular, undefined) {
    var ns = 'sha.membership.profile.edit';
    var module = root.ns(ns);
    var profile = window.profile;
    /**
     * Init module
     */
    module.init = function () {
        var self = this;
        this.country = $("#countrytext");
        this.city = $("#citytext");
        this.state = $("#statetext");
        this.countryId = $("#country_id");
        this.cityId = $("#city_id");
        this.stateId = $("#state_id");
        this.statename = $("#statetext");
        this.countryname = $("#countrytext");

        this.setCountryId = function (value) {
            this.countryId.val(value);
        };
        this.setCityId = function (value) {
            this.cityId.val(value);
        };
        this.setStateId = function (value) {
            this.stateId.val(value);
        };
        this.setStateName = function (value) {
            this.statename.val(value);
        };
        this.setCountryName = function (value) {
            this.countryname.val(value);
        };

        this.country.autocomplete({
            serviceUrl: root.baseUrl('data/countries'),
            transformResult: function (response) {
                var records = JSON.parse(response).records;
                return {
                    suggestions: $.map(records, function (dataItem) {
                        return { value: dataItem.name, data: dataItem.id, ios2: dataItem.iso2, iso3: dataItem.iso3, flag: dataItem.flag};
                    })
                };
            },
            onSelect: function (c) {
                self.setCountryId(c.data);
            },
            onInvalidateSelection: function () {
                self.setCountryId('');
            }
        });

        this.city.autocomplete({
            serviceUrl: root.baseUrl('data/cities'),
            transformResult: function (response) {
                var records = JSON.parse(response).records;
                return {
                    suggestions: $.map(records, function (dataItem) {
                        return { value: dataItem.name, data: dataItem.id, country: dataItem.country, state: dataItem.state};
                    })
                };
            },
            onSelect: function (c) {
                self.setCityId(c.data);
                if (c.state) {
                    self.setStateId(c.state.id);
                    self.setStateName(c.state.name);
                }
                if (c.country) {
                    self.setCountryId(c.country.id);
                    self.setCountryName(c.country.name);
                }
            },
            onInvalidateSelection: function () {
                self.setCityId('');
            }
        });

        this.state.autocomplete({
            serviceUrl: root.baseUrl('data/states'),
            transformResult: function (response) {
                var records = JSON.parse(response).records;
                return {
                    suggestions: $.map(records, function (dataItem) {
                        return { value: dataItem.name, data: dataItem.id, country_id: dataItem.country_id};
                    })
                };
            },
            onSelect: function (c) {
                self.setStateId(c.data);
            },
            onInvalidateSelection: function () {
                self.setStateId('');
            }
        });

        // Add markdown for About me
        $("textarea#about").markdown({autofocus:false,savable:false});
    };

    /**
     * Initalize
     */
    $(function () {
        module.init();
    });

    return module;
})(jQuery, PhalconEye, angular);
