;(function(window, root, angular){
    angular.module('sha.membership', ['ngResource','ui.bootstrap','sha.imagecrop','angularFileUpload','sha.util'])
        .config(function($interpolateProvider) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');
        })

        .factory('MemberService', [
            '$q',
            '$resource',
            function($q, $resource){
                var service = {};
                var profile = window.profile;
                var url = root.baseUrl('profile/avatar');
                var rs = $resource(url, null, {
                    upload:  {
                        method:'PUT'
                    },
                    remove: {
                        method:'DELETE'
                    }
                });
                /**
                 * Upload avatar for member
                 * @param img Canvas
                 */
                service.uploadAvatar = function(img, filename){
                    var defer = $q.defer();
                    var data = img.toDataURL('image/png');
                    var userId = profile.user_id;
                    var params = {id: userId};
                    var payload = {
                        avatar: data,
                        filename: filename
                    };
                    rs.upload(params, payload, function(result){
                        defer.resolve(result);
                    });

                    return defer.promise;
                };

                service.deleteAvatar = function(){
                    var defer = $q.defer();
                    var params = {};
                    rs.remove(params, function(result){
                        defer.resolve(result);
                    });

                    return defer.promise;
                };
                return service;
            }
        ])
        .controller('AvatarCtrl', ['$scope','MemberService', 'ImageCropService', sha.membership.profile.avatar.Controller])

        .directive('imagepreview', [
            '$timeout',
            function($timeout){
            return {
                restrict: "A",
                scope: true,
                link: function(scope, el, attrs){
                    $timeout(function(){
                        var img = scope.$eval(attrs.imagepreview);
                        if(img){
                            el.find('.details').html(img);
                        }
                    },100);
                }
            };
        }]);
})(window, PhalconEye, angular);