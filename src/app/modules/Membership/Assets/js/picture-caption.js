(function ($, core) {
    var ns = 'sha.album.Photo';
    var module = core.ns(ns);

    module.delete = function (id, obj) {
        var url = core.baseUrl('picture-caption/photos/' + id);
        $.ajax(url, {
            data: {},
            type: 'POST',
            dataType:'json',
            success: function(res){
                if(res.status ===true){
                    $(obj).parents('li').remove();
                }
            }
        });
    };

    function init() {
        $(".fancybox").fancybox();
    }
    $(function () {
        init();
    });

    return module;
})(jQuery, PhalconEye);
