/**
 * Created by Quynh on 7/23/14.
 */
(function ($, core) {
    var ns = 'sha.musicJackpot';
    var module = core.ns(ns);
    module.preview = function(url){
        $.ajax(url, {
            success: function(res){
                $('#video-placeholder').html(res);
            }
        });
    }

    $(function () {
        $('.icon-youtube-play').click(function(){
           var url = $(this).data('preview');
            console.log('url:', url);
            if(!url){
                return;
            }
            module.preview(url);
        });
    });
    return module;
})(jQuery, PhalconEye);
