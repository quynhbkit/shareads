/**
 * Created by Quynh on 7/23/14.
 */
(function ($, core) {
    var ns = 'sha.form';
    var form = core.ns(ns);

    function FormHelper(f) {
        var self = this;
        this.form = f;
        this.chkSelector = ':checkbox[name="chk[]"]';
        this.chkAllSelector = ':checkbox[name="chkAll"]';
        this.items = $(this.chkSelector, this.form);
        this.chkAll = $(this.chkAllSelector, this.form);
    }

    FormHelper.prototype ={
        init: function(){
            var self = this;
            this.items.click(function () {
                self.itemCheck();
            });

            this.chkAll.click(function () {
                self.checkAll(this.checked);
            });

            $('*[data-form-action="delete"]').click(function(){
                var action = $(this).data('action');
                self.submit(action, true, true);
            });
        },

        checkAll: function (value) {
            this.items.each(function(){
                this.checked = value;
            });
        },

        itemCheck: function(){
            this.chkAll.get(0).checked = this.hasCheckedAll();
        },

        hasChecked: function(){
            return ($(this.chkSelector+':checked', this.form).length > 0);
        },

        hasCheckedAll: function(){
            return ($(this.chkSelector+':checked', this.form).length === this.items.length);
        },

        /**
         * Submit form
         *
         * @param action URL
         * @param required Require to select an item
         * @param confirm show confirmation dialog?
         */
        submit: function(action, required, confirm){
            var self = this;
            if(required && !this.hasChecked()){
                this.alert('Please select an item');
                return;
            }
            function submit(){
                self.form.attr('action', action);
                self.form.get(0).submit();
            }
            if(!confirm){
                return submit();
            }
            this.confirm('Are you sure you want to delete the selected items?', function(result){
                if(!result){
                    return false;
                }
                return submit();
            });
        },

        confirm: function(message, cb){
            bootbox.confirm(message, function(result){
                cb(result);
            });
        },

        alert: function(message, cb){
            bootbox.alert(message, cb);
        }
    };
    form.Helper = FormHelper;

    $(function () {
        $('form[data-form-list]').each(function () {
            (new FormHelper($(this))).init();
        });
    });
    return form;
})(jQuery, PhalconEye);
