;(function(root, angular, undefined){
    angular.module('sha.imagecrop', [])
    .factory('ImageCropService', [
        '$q',
        '$resource',
        '$modal',
        function($q, $resource, $modal) {
            var defaults = {
                file: null
            };
            return {
                open: function(file) {
                    var deferred = $q.defer();
                    var controller = function($scope, $modalInstance, options) {
                        $scope.options = options;
                        FileAPI.getInfo(file, function(err, info) {
                            var basicSize = 600;
                            var aspectRatio = 1;

                            var height = 600;
                            var width = 600;
                            console.log('info widht, h:', info.width, info.height);
                            if (info.width > info.height) {
                                if (info.width < width) {
                                    width = info.width;
                                }
                                aspectRatio = info.width / info.height;
                                height = Math.round(width / aspectRatio);
                            } else {
                                if (info.height < height) {
                                    height = info.height;
                                }
                                aspectRatio = info.height / info.width;
                                width = Math.round(height / aspectRatio);
                            }

                            var w = 200;
                            var h = 200;
                            var aspectRatioThumb = w / h;
                            var size = [w, h];
                            var area = [0, 0, w, h];
                            console.log('widht, height:', width, height);
                            FileAPI.Image(file)
                                .preview(width, height)
                                .get(function(err, img) {
                                    $('.modal-body').append(img);
                                    resized = img;
                                    $(img).Jcrop({
                                        trackDocument: true,
                                        aspectRatio: aspectRatioThumb,
                                        minSize: size,
                                        setSelect: area,
                                        onSelect: function(x) {
                                            coord = x;
                                        }
                                    });
                                });
                        });

                        $scope.close = function(result) {
                            return $modalInstance.close(result);
                        };
                    };

                    var w = 800;
                    var h = 700;
                    var top = 200;
                    var self = this;

                    var coord = null;
                    var resized = null;

                    var win = $modal.open({
                        backdrop: false,
                        controller: controller,
                        keyboard: true,
                        size:'lg',
                        windowClass: 'modal fade in crop-image',
                        template:'<div class="modal-dialog">'+
                            '<div class="modal-content">'+
                            '   <div class="modal-header">'+
                            '       <button type="button" class="close" ng-click="close()" aria-hidden="true">&times;</button>'+
                            '       <h4 class="modal-title">Crop avatar</h4>'+
                            '   </div>'+
                            '   <div class="modal-body" style="overflow:auto; text-align: center">'+
                            '   </div>'+
                            '   <div class="modal-footer">'+
                            '       <button type="button" class="btn btn-primary" ng-click="options.okClick()">OK</button>'+
                            '       <button type="button" class="btn btn-primary" ng-click="close(\'close\')">Close</button>'+
                            '   </div>'+
                            '</div>'+
                            '</div>',

                        resolve: {
                            options: function() {
                                return {
                                    title: 'Create thumbnail',
                                    file: file,
                                    top: top,
                                    width: w,
                                    height: h,
                                    okClick: function() {
                                        doSelect();
                                    }
                                };
                            }
                        }
                    });

                    var doSelect = function doSelect() {
                        FileAPI.Image(resized)
                            .crop(coord.x, coord.y, coord.w, coord.h)
                            .get(function(err, img) {
                                deferred.resolve(img);
                                win.close();
                            });
                    };

                    return deferred.promise;
                },
                crop: function(file) {
                    var deferred = $q.defer();
                    var self = this;
                    self.open(file).then(function(rs) {
                        deferred.resolve(rs);
                    });
                    return deferred.promise;
                }
            };
        }
    ]);
})(PhalconEye, angular);

