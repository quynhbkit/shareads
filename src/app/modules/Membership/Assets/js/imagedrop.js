;
(function (root, angular, undefined) {
    angular.module('sha.imagecrop', [])

    .directive('imagedrop', ['$parse', function ($parse) {
        return {
            restrict: "EA",
            link: function (scope, element, attrs) {
                //The on-image-drop event attribute
                var onImageDrop = $parse(attrs.onImageDrop);

                //When an item is dragged over the document, add .dragOver to the body
                var onDragOver = function (e) {
                    e.preventDefault();
                    element.addClass("drag-hover");
                };

                //When the user leaves the window, cancels the drag or drops the item
                var onDragEnd = function (e) {
                    e.preventDefault();
                    element.removeClass("drag-hover");
                };

                //When files is dropped on the overlay
                var loadFile = function (files, e) {
                    scope.$apply(onImageDrop(scope, {
                        $files: files,
                        $event: e
                    }));
                };

                //Dragging begins on the document (shows the overlay)
                element.bind("dragover", onDragOver);

                //Dragging ends on the overlay, which takes the whole window
                element.bind("dragleave", onDragEnd)
                    .bind("drop", function (e) {
                        onDragEnd(e);
                        var list = e.originalEvent.dataTransfer.files;
                        var files = [];
                        for (var i = 0; i < list.length; i++) {
                            files.push(list[i]);
                        }
                        loadFile(files, e);
                    });
            }
        };
    }]);
})(PhalconEye, angular);

