/**
 * Created by Quynh on 7/23/14.
 */
(function ($, core) {
    var ns = 'sha.template';
    var template = core.ns(ns);

    function TemplatePicture() {
        this.type = null;
        this.dataUrl = $('body').data('theme-url');
        this.updateUrl = $('body').data('theme-update-url');
        if(this.dataUrl){
            this.init();
        }
    }

    TemplatePicture.prototype ={
        init: function(){
            this.bindEvents();
        },

        bindEvents: function(){
            var self = this;
            $('a[data-template-pic]').bind('click', function(){
                var type = $(this).data('template-pic');
                self.onClick(type);
            });
            $('body').on('click', '.pagination a', function(e){
                var url = $(this).attr('href');
                self.loadTemplatePictures(url);
                e.preventDefault();
            });

            $('body').on('click', '.bootbox-body button[data-select]', function(e){
                var id= $(this).data('select');
                if(id===''){
                    return false;
                }
                self.updateTemplateData(id);
            });
        },

        updateTemplateData: function(id){
            var self = this;
            var url = this.updateUrl;
            var data ={param: this.type, data: id, type: 'picture'};
            $.ajax({
                url: url,
                method:'POST',
                data: data,
                dataType:'json'
            }).done(function(json){
                self.alert('Updated successfully')
            });
        },

        loadTemplatePictures: function(url){
            var body = $('div.bootbox-body');
            $.ajax({
                url: url
            }).done(function(html){
                body.html(html);
            });
        },

        onClick: function(type){
            var self = this;
            this.type = type;
            var url = this.dataUrl + type;
            $.ajax({
                url: url
            }).done(function(html){
                self.openDialog(html);
            });
        },

        openDialog: function(html){
            bootbox.dialog({
                message: html,
                title: "Customize template",
                className:'lg',
                buttons: {
                    Close: {
                        label: "Close",
                        className: "btn-success",
                        callback: function(){

                        }
                    }
                }
            }).find('div.modal-dialog').addClass('modal-lg');
        },

        /**
         * Submit form
         *
         * @param action URL
         * @param required Require to select an item
         * @param confirm show confirmation dialog?
         */
        submit: function(action, required, confirm){

        },

        confirm: function(message, cb){
            bootbox.confirm(message, function(result){
                cb(result);
            });
        },

        alert: function(message, cb){
            bootbox.alert(message, cb);
        }
    };
    template.Picture = TemplatePicture;

    $(function () {
        new TemplatePicture();
    });
    return template;
})(jQuery, PhalconEye);
