;
(function ($, root, angular, undefined) {
    var util = root.ns('sha.util');
    util.createCanvasFromImageUrl = function (src, w, h) {
        var canvas = document.createElement('canvas');
        canvas.width = w;
        canvas.height = h;
        var image = new Image();
        image.src = src;
        image.onload = function () {
            canvas.getContext('2d').drawImage(image, 0, 0);
        };
        return canvas;
    };

    angular.module('sha.util', ['ui.bootstrap'])
        .config(function ($httpProvider) {
            $httpProvider.interceptors.push('loadingStatusInterceptor');
        })

        .directive('loadingWidget', function () {
            return {
                link: function ($scope, $element, attrs) {
                    var show = function () {
                        $element.show();
                    };
                    var hide = function () {
                        $element.hide();
                    };
                    $scope.$on('loadingStatusActive', show);
                    $scope.$on('loadingStatusInactive', hide);
                    hide();
                }
            };
        })

        .factory('loadingStatusInterceptor', function ($q, $rootScope, $timeout) {
            var activeRequests = 0;
            var started = function () {
                if (activeRequests === 0) {
                    $rootScope.$broadcast('loadingStatusActive');
                }
                activeRequests++;
            };
            var ended = function () {
                activeRequests--;
                if (activeRequests === 0) {
                    $rootScope.$broadcast('loadingStatusInactive');
                }
            };
            return {
                request: function (config) {
                    started();
                    return config || $q.when(config);
                },
                response: function (response) {
                    ended();
                    return response || $q.when(response);
                },
                responseError: function (rejection) {
                    ended();
                    return $q.reject(rejection);
                }
            };
        })

        /**
         * Stop Event Bubbling
         */
        .directive('stopEvent', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    element.bind(attr.stopEvent, function (e) {
                        e.stopPropagation();
                    });
                }
            };
        })

        .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEnter);
                        });
                        event.preventDefault();
                    }
                });
            };
        })

        /**
         * Alert dialog service
         */
        .factory('MessageBox', [
            '$modal',
            function ($modal) {
                var controller = function ($scope, $modalInstance, options) {
                    $scope.options = options;
                    $scope.close = function (result) {
                        return $modalInstance.close(result);
                    };
                };

                return {
                    show: function (options) {
                        options = options || {};
                        return $modal.open({
                            controller: controller,
                            keyboard: true,
                            templateUrl: 'common/alert.tpl.html',
                            windowClass: 'alert-dialog',
                            resolve: {
                                options: function () {
                                    return options;
                                }
                            }
                        });
                    },

                    alert: function (options) {
                        options.title = 'Alert';
                        return this.show(options);
                    },

                    confirm: function (options) {
                        return $modal.open({
                            controller: controller,
                            dialogFade: true,
                            keyboard: true,
                            templateUrl: 'common/confirm.tpl.html',
                            windowClass: 'alert-dialog',
                            resolve: {
                                options: function () {
                                    return options;
                                }
                            }
                        });
                    }
                };
            }
        ]);

    return util;
})(jQuery, PhalconEye, angular);
