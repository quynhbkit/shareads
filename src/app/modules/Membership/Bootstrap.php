<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Membership;

use Engine\Bootstrap as EngineBootstrap;

/**
 * Bootstrap for Membership.
 *
 * @category PhalconEye\Module
 * @package  Module
 */
class Bootstrap extends EngineBootstrap
{
    protected $_moduleName = "Membership";

    protected function _initFilter(){

    }
//    public function registerServices(){
//        parent::registerServices();
//
//        $di = $this->getDI();
//        $volt = $di->getShared('volt');
//        $compiler = $volt->getCompiler();
//        $compiler->addFilter('markdown',
//            function($resolvedArgs, $exprArgs) {
//                return "Markdown::filter(".$resolvedArgs.")";
//            }
//        );
//    }
}