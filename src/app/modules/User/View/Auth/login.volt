{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

{% extends "Core/View/layouts/main.volt" %}

{% block title %}{{ 'Login'|i18n }}{% endblock %}
{% block content %}
{{ form.setAttribute('class','form-signin').openTag() }}
    {{ partial(form.getErrorsView(), ['form': form]) }}
    <h2 class="form-signin-heading">{{ 'sign in now' | i18n }}</h2>
    <div class="login-wrap">
        <input type="text" name="login" class="form-control" placeholder="User ID" autofocus>
        <input type="password" name="password" class="form-control" placeholder="Password">
        <label class="checkbox">
            <input type="checkbox" value="remember-me"> {{ 'Remember me' | i18n }}
            <span class="pull-right">
                <a data-toggle="modal" href="#myModal"> {{ 'Forgot Password?' | i18n }}</a>
            </span>
        </label>
        <button class="btn btn-lg btn-login btn-block" type="submit">{{ 'Sign in' | i18n }}</button>
        <p>{{ 'or you can sign in via social network' | i18n }}</p>
        <div class="login-social-link">
            <a href="#" class="facebook">
                <i class="icon-facebook"></i>
                {{ 'Facebook' | i18n }}
            </a>
            <a href="#" class="twitter">
                <i class="icon-twitter"></i>
                {{ 'Twitter' | i18n }}
            </a>
        </div>
        <div class="registration">
            {{ "Don't have an account yet?"| i18n }}
            <a class="" href="{{ url('register') }}">
                {{ 'Create an account' | i18n }}
            </a>
        </div>
    </div>
{{ form.closeTag() }}
{% endblock %}
